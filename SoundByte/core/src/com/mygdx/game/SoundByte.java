/**
 * @author Griffins & @K—SoundByte™
 */

package com.mygdx.game;

public class SoundByte extends BaseGame {
    public static kUtils.kCallback callback;

    //@K—callback to modify app after initial launch.
    //Necessary because it must launch with a resizable=true GUI *first* to respect the Dock bounds on Mac...
    //...and other windows (all while maintaining aspect ratio).
    //...Then, after launch, lock the GUI (prevent resizing, that is).
    public SoundByte(kUtils.kCallback callback){this.callback=callback;}


    public void create()
    {
        super.create();

        setActiveScreen( new Launch() );
    }
}
