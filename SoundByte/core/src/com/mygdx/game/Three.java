/**
 * @author Griffins & @K—SoundByte™
 */

package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
//import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
//import sun.font.TextRecord;
//import sun.jvm.hotspot.gc.shared.Space;

//import java.awt.*;
//import java.security.Key;
//import java.text.DecimalFormat;
import java.util.Arrays;
//import java.util.Timer;

//@K—for variables to stop the music, primarily
//import com.mygdx.game.MenuScreen.*;
import static com.badlogic.gdx.utils.Timer.schedule;
import static com.mygdx.game.BaseGame.setActiveScreen;
import static com.mygdx.game.Launch.*;
import static com.mygdx.game.Tutorial.*;


public class Three extends BaseScreen{

    // The sounds
//    static Music soundA5;
//    // These are not used in this level
//    static Music soundB5;
//    static Music soundC6;
//    static Music soundD6;
//    static Music soundE6;
//    static Music soundKick;
//    static Music soundHat;
//    static Music soundBass;
//    static Music soundSnare;
//    static Music melodyPattern;
//
////    private Music beat1;
//        static Music drumPattern;

    // The QButton bound to soundA5
//    private Button QButton;

    // total length of soundtrack
//    private float total;

    // dt since last sound
//    private int time;

    private float counterTimer;
    //    private float Timer;
    private float Timer1;
    private Label TimeLeft;
    private Label beatTimeLeft;

    private Label stepLabel;
    private int step;

    private int beatTime;

//    private boolean hatAdded;
//    private boolean BassAdded;

    //    private boolean kickPatternPlay;
//    private boolean  snarePatternPlay;
//    private boolean  hatandbassPatternPLay;
    private boolean drumPatternPlay;
    private boolean melodyPatternPlay ;

    ///RESTART
//    private boolean restart;





    // dt allowed between sounds

    //@K—made "final" to prevent accidental modification; also as it's not prone to alteration later in the sourcecode
    private final int delay = 50 / Preferences.getDifficulty();

    private float delay1; //Delay one second

    // whether the game should play the sounds to match
    private boolean playback;

    // starts listening to your key presses
    private boolean start;

    private boolean counterStart;

    // checks to see if you made an attempt at matching
    private boolean attempted;


    // the number of sounds to play
    private int countKick;
    private int countSnare;
    private int countHat;
    private int countBass;
    private int countMelody;

    // array of sounds matched

    private int[] matchedKick;
    private int[] matchedSnare;
    private int[] matchedHat;
    private int[] matchedBass;
    private int[] matchedMelody;

    // number of sounds matched
    private int correctKick;
    private int correctSnare;
    private int correctHat;
    private int correctBass;
    private int correctMelody;

    // Level Complete
    private boolean complete;

//    private boolean complete2;

    // The onscreen timer
//    private Label timerLabel;

    private Label sequenceLabel;

    //———————————————————————————————————————————————————//

    //@K—declare "batch" var for sprite rendering (SpriteBatch rendering, that is)
    SpriteBatch batch;

    //@K—Level graphic/header
    Texture levelTexture;
    Sprite levelSprite;

    //@K—Level graphic/header
    Texture levelHeaderTexture;
    Sprite levelHeaderSprite;


    //@K—circle button 1 for sound-clicking and matching MELODY-BASED EL'TS
    Texture circleTexture1;
    Sprite circleSprite1;

    //@K—circle button 2 for sound-clicking and matching MELODY-BASED EL'TS
    Texture circleTexture2;
    Sprite circleSprite2;

    //@K—circle button 3 for sound-clicking and matching MELODY-BASED EL'TS
    Texture circleTexture3;
    Sprite circleSprite3;

    //@K—circle button 4 for sound-clicking and matching MELODY-BASED EL'TS
    Texture circleTexture4;
    Sprite circleSprite4;

    //@K—square button 1 for sound-clicking and matching ONE-SHOT-BASED EL'TS
    Texture squareTexture1;
    Sprite squareSprite1;

    //@K—square button 2 for sound-clicking and matching ONE-SHOT-BASED EL'TS
    Texture squareTexture2;
    Sprite squareSprite2;

    //@K—square button 3 for sound-clicking and matching ONE-SHOT-BASED EL'TS
    Texture squareTexture3;
    Sprite squareSprite3;

    //@K—square button 4 for sound-clicking and matching ONE-SHOT-BASED EL'TS
    Texture squareTexture4;
    Sprite squareSprite4;

    //@K—mid-centered spectrum ani
    Sprite spectrumSprite;
    TextureAtlas spectrumAtlas;
    TextureRegion spectrumRegion;
    Animation spectrumAni;
//    int spectrumCurrentFrame; //@K—For single-frame render here, not infinitely looping animation.

    //@K—Bottom mid-centered waveform ani
    Sprite waveformSprite;
    TextureAtlas waveformAtlas;
    TextureRegion waveformRegion;
    Animation waveformAni;
//    int waveformCurrentFrame; //@K—For single-frame render here, not infinitely looping animation.

    //@K—Bottom mid-centered waveform ani—ONLY WHEN CLICKED/ACTIVATED (UPON EVENTLISTENER ACTIVITY)
    Sprite waveformClickedSprite;
    TextureAtlas waveformClickedAtlas;
    TextureRegion waveformClickedRegion;
    Animation waveformClickedAni;
    boolean waveformClickedStatus=false;
//    int waveformCurrentFrame; //@K—For single-frame render here, not infinitely looping animation.

    //@K—frame/image to render at particular point during spectrum ani
    private TextureRegion spectrumCurrentFrame;

    //@K—frame/image to render at particular point during waveform ani
//    private TextureRegion waveformCurrentFrame;

    //@K—frame/image to render at particular point during waveform ani UPON CLICK
//    private TextureRegion waveformClickedCurrentFrame;


    //@K—timed passed regarding frames
    private float timePassed = 0f;

    //@K—timed passed regarding frames—begin counting once clicked so as to begin at frame 0
//    private float waveformTimePassed = 0f;

    //@K—positioning variables for anis/animations
//    private float aniX, aniY;

    //@K—back arrow graphic/header
    Texture backArrowTexture;
    Sprite backArrowSprite;



    //—————————————————//

    //@K—Q letter graphic
    Texture qLetterTexture;
    Sprite qLetterSprite;

    //@K—W letter graphic
    Texture wLetterTexture;
    Sprite wLetterSprite;

    //@K—E letter graphic
    Texture eLetterTexture;
    Sprite eLetterSprite;

    //@K—R letter graphic
    Texture rLetterTexture;
    Sprite rLetterSprite;

    //@K—U letter graphic
    Texture uLetterTexture;
    Sprite uLetterSprite;

    //@K—I letter graphic
    Texture iLetterTexture;
    Sprite iLetterSprite;

    //@K—O letter graphic
    Texture oLetterTexture;
    Sprite oLetterSprite;

    //@K—P letter graphic
    Texture pLetterTexture;
    Sprite pLetterSprite;

    //@K—————ANNOUNCER DIALOGUES for Tutorial/Experiment/Levels
//    static Music musicNowPlayingByte1;
//    static Music musicNowPlayingByte2;
//    static Music musicNowPlayingByte3;
//    static Music musicNowPlayingByte4;
//    static Music musicNowPlayingByte5;
//    static Music musicNowPlayingByte6;
//    static Music musicNowPlayingByte7;
//    static Music musicNowPlayingByte8;
//    static Music music3AndAHalfMinutes;
//    static Music music5Minutes;
//    static Music music7Minutes;
//    static Music musicCountdown;
//    static Music musicExperiment;
//    static Music musicSoundset;
//    static Music musicTutorial;
//    static Music musicUnsuccessful; //@K—user has *unsuccessfully* matched and constructed a given Byte
//    static Music musicSuccessful; //@K—user has *successfully* matched and constructed the SoundByte
//    static Music musicVictory; //@K—user *successfully* matched and constructed the SoundByte + all is complete
//
//    //✳️✳️✳️
//    //✳️✳️✳️
//    //✳️✳️✳️
//    //✳️✳️✳️
//    //@K—MY ACTUAL MUSIC! CAN RESERVE FOR LEVEL 3 IF NEED BE BUT NOW THIS IS ACROSS ALL LEVELS AND TUT
//    static Music musicKSoundByte1;
//    static Music musicKByte1;
//    static Music musicKByte2;
//    static Music musicKByte3;
//    static Music musicKByte4;
//    static Music musicKByte5;
//    static Music musicKByte6;
//    static Music musicKByte7;
//    static Music musicKByte8;
//    //@K—ACTUAL single-shot sounds AND melody SOUNDS used in each byte
//    static Music musicKByte1Sound;
//    static Music musicKByte2Sound;
//    static Music musicKByte3Sound;
//    static Music musicKByte4Sound;
//    static Music musicKByte5Sound;
//    static Music musicKByte6Sound;
//    static Music musicKByte7Sound;
//    static Music musicKByte8Sound;
//
//    //@K—if user successfully matches; variables for Bytes 1-8 below
//    static boolean kSuccessByte1=false;
//    static boolean kSuccessByte2=false;
//    static boolean kSuccessByte3=false;
//    static boolean kSuccessByte4=false;
//    static boolean kSuccessByte5=false;
//    static boolean kSuccessByte6=false;
//    static boolean kSuccessByte7=false;
//    static boolean kSuccessByte8=false; //@K—declare victory in the 8th else clause if kSuccessByte8==true+play user-constructed byte
//    static boolean kVictory=false;//@K—user *successfully* matched and constructed the SoundByte + all is complete



    //——————————————————————————//
    //——————————————————————————//
    //——————————————————————————//

    public void initialize() {

        //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
        Timer.instance().clear();

        //@K—need a try/catch here as the sounds generate a NullPtrException if they haven't been activated/clicked on yet, meaning they don't "exist" and are null
//        try
//        {
//            //@K—if immediately clicked into—no delay here, as audio begins immediately when main menu screen/app is opened, effectively
//            musicIntro.stop();
//
//            //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
//            musicIntroAmbience.stop();
//
//            //@K—stop all sounds as part of the actual patterns/bytes
//            soundA5.stop();
//            soundB5.stop();
//            soundC6.stop();
//            soundE6.stop();
//            soundKick.stop();
//            soundSnare.stop();
//            soundBass.stop();
//            soundHat.stop();
//        } catch (Exception e){e.printStackTrace();}


        //@K—above still generates nullptr—perform if-else clause checking

        //✳️✳️@K—CLEAR ALL TASKS IMMEDIATELY SO NO AUDIO PLAYS CROSS-LEVEL GIVEN POSSIBLE DELAY WHEN JUMPING IN AND OUT OF LEVELS
//        if(kTimer!=null){kTimer.stop();kTimer.clear();}

        //@K—if immediately clicked into this particular/current screen—no delay here, as audio begins immediately when main menu screen/app is opened, effectively
        if (musicIntro != null) musicIntro.stop();

        //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
        if (musicIntroAmbience != null) musicIntroAmbience.stop();

        //@K—stop all sounds as part of the actual patterns/bytes—ORIGINALS
        if (soundA5 != null) soundA5.stop();
        if (soundB5 != null) soundB5.stop();
        if (soundC6 != null) soundC6.stop();
        if (soundE6 != null) soundE6.stop();
        if (soundKick != null) soundKick.stop();
        if (soundSnare != null) soundSnare.stop();
        if (soundBass != null) soundBass.stop();
        if (soundHat != null) soundHat.stop();


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—stop all sounds as part of the actual patterns/bytes—MY/K's MUSIC
        if (musicKSoundByte1 != null) musicKSoundByte1.stop();
        if (musicKByte1 != null) musicKByte1.stop();
        if (musicKByte2 != null) musicKByte2.stop();
        if (musicKByte3 != null) musicKByte3.stop();
        if (musicKByte4 != null) musicKByte4.stop();
        if (musicKByte5 != null) musicKByte5.stop();
        if (musicKByte6 != null) musicKByte6.stop();
        if (musicKByte7 != null) musicKByte7.stop();
        if (musicKByte8 != null) musicKByte8.stop();
        if (musicKByte2Sound != null) musicKByte2Sound.stop();
        if (musicKByte3Sound != null) musicKByte3Sound.stop();
        if (musicKByte1Sound != null) musicKByte1Sound.stop();
        if (musicKByte4Sound != null) musicKByte4Sound.stop();
        if (musicKByte5Sound != null) musicKByte5Sound.stop();
        if (musicKByte6Sound != null) musicKByte6Sound.stop();
        if (musicKByte7Sound != null) musicKByte7Sound.stop();
        if (musicKByte8Sound != null) musicKByte8Sound.stop();



        //@K—STOP ANY ANNOUNCER DIALOGUE + BEAT AUDIO ACROSS LEVELS/EXPERIMENT/TUTORIAL——————//


        if (musicNowPlayingByte1 != null) musicNowPlayingByte1.stop();
        if (musicNowPlayingByte2 != null) musicNowPlayingByte2.stop();
        if (musicNowPlayingByte3 != null) musicNowPlayingByte3.stop();
        if (musicNowPlayingByte4 != null) musicNowPlayingByte4.stop();
        if (musicNowPlayingByte5 != null) musicNowPlayingByte5.stop();
        if (musicNowPlayingByte6 != null) musicNowPlayingByte6.stop();
        if (musicNowPlayingByte7 != null) musicNowPlayingByte7.stop();
        if (musicNowPlayingByte8 != null) musicNowPlayingByte8.stop();
        if (music3AndAHalfMinutes != null) music3AndAHalfMinutes.stop();
        if (music5Minutes != null) music5Minutes.stop();
        if (music7Minutes != null) music7Minutes.stop();
        if (musicCountdown != null) musicCountdown.stop();
        if (musicExperiment != null) musicExperiment.stop();
        if (musicSoundset != null) musicSoundset.stop();
        if (musicTutorial != null) musicTutorial.stop();
        if (musicUnsuccessful != null) musicUnsuccessful.stop();
        if (musicSuccessful != null) musicSuccessful.stop();
        if (musicVictory != null) musicVictory.stop();
        //——————//


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—my music, initialized here—ONLY IN TUTORIAL + LEVELS + EXPERIMENT (WITHOUT THE RUNNABLES/delays BELOW AS NO INTRO AUDIO, OF COURSE), AS THIS IS WHERE the music is featured
        musicKSoundByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_SOUNDBYTE_ONE.mp3"));
        musicKByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_ONE.mp3"));
        musicKByte2 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_TWO.mp3"));
        musicKByte3 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_THREE.mp3"));
        musicKByte4 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FOUR.mp3"));
        musicKByte5 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FIVE.mp3"));
        musicKByte6 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SIX.mp3"));
        musicKByte7 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SEVEN.mp3"));
        musicKByte8 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_EIGHT.mp3"));
        //@K—actual melody/one-shot sounds used in bytes above, stemmed out and appended with "Sound"
        musicKByte1Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_ONE_SOUND.mp3"));
        musicKByte2Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_TWO_SOUND.mp3"));
        musicKByte3Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_THREE_SOUND.mp3"));
        musicKByte4Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FOUR_SOUND.mp3"));
        musicKByte5Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FIVE_SOUND.mp3"));
        musicKByte6Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SIX_SOUND.mp3"));
        musicKByte7Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SEVEN_SOUND.mp3"));
        musicKByte8Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_EIGHT_SOUND.mp3"));


        //@K—IMPLEMENTATION OF DIALOGUE for this specific class (and use static declaration to .stop() these sounds like the above)
        //@K—KEEP THE STATIC IMPLEMENTATIONS **ONLY** IN TUTORIAL.JAVA (THIS IS WHERE THE STATIC VARIABLES ARE SET...
        //... SO AS TO .stop() THEM STATICALLY IN ANOTHER SCREEN—I.E., SIMPLY DON'T INCLUDE CLASS VARIABLES...
        //... FOR THESE **AND** THE ABOVE SOUNDS (ALL SOUNDS, ACTUALLY, AS PART OF THE ACTUAL BEATS/MUSIC)...
        //... IN OTHER CLASSES—ONLY KEEP THEM STATIC HERE (and commented out in other .java files' classes at the top, as class vars) OR THEY'LL BE OVERWRITTEN.
        //... I arbitrarily chose this class to keep them static—but only have them as class vars here for now.

        musicNowPlayingByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_ONE.mp3"));
        musicNowPlayingByte2 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_TWO.mp3"));
        musicNowPlayingByte3 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_THREE.mp3"));
        musicNowPlayingByte4 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_FOUR.mp3"));
        musicNowPlayingByte5 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_FIVE.mp3"));
        musicNowPlayingByte6 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_SIX.mp3"));
        musicNowPlayingByte7 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_SEVEN.mp3"));
        musicNowPlayingByte8 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_EIGHT.mp3"));
        music3AndAHalfMinutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_THREE_AND_A_HALF_MINUTES.mp3"));
        music5Minutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_FIVE_MINUTES.mp3"));
        music7Minutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_SEVEN_MINUTES.mp3"));
        musicCountdown = Gdx.audio.newMusic(Gdx.files.internal("K/K_COUNTDOWN.mp3"));
        musicExperiment = Gdx.audio.newMusic(Gdx.files.internal("K/K_EXPERIMENT.mp3"));
        musicSoundset = Gdx.audio.newMusic(Gdx.files.internal("K/K_SOUNDSET.mp3"));
        musicTutorial = Gdx.audio.newMusic(Gdx.files.internal("K/K_TUTORIAL.mp3"));
        musicUnsuccessful = Gdx.audio.newMusic(Gdx.files.internal("K/K_UNSUCCESSFUL.mp3"));
        musicSuccessful = Gdx.audio.newMusic(Gdx.files.internal("K/K_SUCCESSFUL.mp3"));
        musicVictory = Gdx.audio.newMusic(Gdx.files.internal("K/K_VICTORY.mp3"));



        //✳️BEGIN MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️BEGIN MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️BEGIN MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️BEGIN MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️BEGIN MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️BEGIN MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️


//        //@K—immediately play LEVEL intro
//        musicTutorial.play();

        //@K—delay 1s for announcer countdown for the level
        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
            @Override
            public void run() {
                musicCountdown.play();
            }
        }, 1);

        //@K—AFTER LEVEL INTRO + COUNTDOWN, DELAY 1 more second + PLAY ACTUAL BEAT/DRUMMPATTERN!
        // (Properly named var must be here, such as drummpattern1, 2, etc. Add these over time upon making new tracks/beats/musical sequences.)
        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
            @Override
            public void run() {
                musicKSoundByte1.play();
            }
        }, 5);

        //———————//

        //@K—AFTER LEVEL INTRO + COUNTDOWN + FULL SOUNDBYTE IS PLAYED, DELAY 1 more second + PLAY "Now Playing Byte #" 8 times, once for each unique/distinct byte to user-match!
        //@K—8 SECONDS LONG TOTAL FOR ENTIRE SOUNDBYTE FOR MY MUSIC/TRACK I/SOUNDBYTE I

        //@K—"Now Playing Byte 1" + DELAY 1s + PLAY BEAT
        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
            @Override
            public void run() {
                musicNowPlayingByte1.play();
            }
        }, 14);

        //@K—"Now Playing Byte 1" + DELAY 1s + PLAY BEAT
        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
            @Override
            public void run() {
                musicKByte1.play();
            }
        }, 18);


        if (kSuccessByte1)
        {
            //@K—PLAY ANNOUNCER SUCCESS VOICE PER-BYTE AFTER 1S DELAY
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicSuccessful.play();
                }
            }, 1);

            //@K—"Now Playing Byte 2" AFTER DELAY 4s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicNowPlayingByte2.play();
                }
            }, 4);

            //@K—ACTUALLY PLAY BYTE 2 AFTER DELAY 5s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicKByte2.play();

                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                    //✳️SO CODE GOES HERE FOR ARRAY CHECKING!
                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                }
            }, 5);
            //✳️@K—REPEAT AGAIN IF UNSUCCESSFUL!
            if(!kSuccessByte2)
            {
                //@K—play announcer "Unsuccessful" audio
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicUnsuccessful.play();
                    }
                }, 1);

                //@K—Repeat the byte
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicKByte2.play();
                    }
                }, 3);

                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //✳️❗SO: IF THEY SUCCESSFULLY MATCH HERE GIVEN THE ARRAY TIMINGS! INCLUDE THIS FOR EACH SECTION!
                //if(THE TIMINGS MATCH){...SET kSuccessByte#=true for the current # byte to exit the loop and move on!}
                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //SO THE DEBUG CODE IS:
                //kSuccessByte#=true;

            }
        }

        //———//

        if (kSuccessByte2)
        {
            //@K—PLAY ANNOUNCER SUCCESS VOICE PER-BYTE AFTER 1S DELAY
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicSuccessful.play();
                }
            }, 1);

            //@K—"Now Playing Byte 3" AFTER DELAY 4s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicNowPlayingByte3.play();
                }
            }, 4);

            //@K—ACTUALLY PLAY BYTE 3 AFTER DELAY 5s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicKByte3.play();

                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                    //✳️SO CODE GOES HERE FOR ARRAY CHECKING!
                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                }
            }, 5);
            //✳️@K—REPEAT AGAIN IF UNSUCCESSFUL!
            if(!kSuccessByte3)
            {
                //@K—play announcer "Unsuccessful" audio
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicUnsuccessful.play();
                    }
                }, 1);

                //@K—Repeat the byte
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicKByte3.play();
                    }
                }, 3);

                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //✳️❗SO: IF THEY SUCCESSFULLY MATCH HERE GIVEN THE ARRAY TIMINGS! INCLUDE THIS FOR EACH SECTION!
                //if(THE TIMINGS MATCH){...SET kSuccessByte#=true for the current # byte to exit the loop and move on!}
                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //SO THE DEBUG CODE IS:
                //kSuccessByte#=true;

            }
        }


        if (kSuccessByte3)
        {
            //@K—PLAY ANNOUNCER SUCCESS VOICE PER-BYTE AFTER 1S DELAY
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicSuccessful.play();
                }
            }, 1);

            //@K—"Now Playing Byte 4" AFTER DELAY 4s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicNowPlayingByte4.play();
                }
            }, 4);

            //@K—ACTUALLY PLAY BYTE 4 AFTER DELAY 5s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicKByte4.play();

                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                    //✳️SO CODE GOES HERE FOR ARRAY CHECKING!
                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                }
            }, 5);
            //✳️@K—REPEAT AGAIN IF UNSUCCESSFUL!
            if(!kSuccessByte4)
            {
                //@K—play announcer "Unsuccessful" audio
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicUnsuccessful.play();
                    }
                }, 1);

                //@K—Repeat the byte
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicKByte4.play();
                    }
                }, 3);

                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //✳️❗SO: IF THEY SUCCESSFULLY MATCH HERE GIVEN THE ARRAY TIMINGS! INCLUDE THIS FOR EACH SECTION!
                //if(THE TIMINGS MATCH){...SET kSuccessByte#=true for the current # byte to exit the loop and move on!}
                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //SO THE DEBUG CODE IS:
                //kSuccessByte#=true;

            }
        }


        if (kSuccessByte4)
        {
            //@K—PLAY ANNOUNCER SUCCESS VOICE PER-BYTE AFTER 1S DELAY
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicSuccessful.play();
                }
            }, 1);

            //@K—"Now Playing Byte 5" AFTER DELAY 4s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicNowPlayingByte5.play();
                }
            }, 4);

            //@K—ACTUALLY PLAY BYTE 5 AFTER DELAY 5s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicKByte5.play();

                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                    //✳️SO CODE GOES HERE FOR ARRAY CHECKING!
                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                }
            }, 5);
            //✳️@K—REPEAT AGAIN IF UNSUCCESSFUL!
            if(!kSuccessByte5)
            {
                //@K—play announcer "Unsuccessful" audio
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicUnsuccessful.play();
                    }
                }, 1);

                //@K—Repeat the byte
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicKByte5.play();
                    }
                }, 3);

                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //✳️❗SO: IF THEY SUCCESSFULLY MATCH HERE GIVEN THE ARRAY TIMINGS! INCLUDE THIS FOR EACH SECTION!
                //if(THE TIMINGS MATCH){...SET kSuccessByte#=true for the current # byte to exit the loop and move on!}
                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //SO THE DEBUG CODE IS:
                //kSuccessByte#=true;

            }
        }


        if (kSuccessByte5)
        {
            //@K—PLAY ANNOUNCER SUCCESS VOICE PER-BYTE AFTER 1S DELAY
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicSuccessful.play();
                }
            }, 1);

            //@K—"Now Playing Byte 6" AFTER DELAY 4s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicNowPlayingByte6.play();
                }
            }, 4);

            //@K—ACTUALLY PLAY BYTE 6 AFTER DELAY 5s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicKByte6.play();

                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                    //✳️SO CODE GOES HERE FOR ARRAY CHECKING!
                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                }
            }, 5);
            //✳️@K—REPEAT AGAIN IF UNSUCCESSFUL!
            if(!kSuccessByte6)
            {
                //@K—play announcer "Unsuccessful" audio
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicUnsuccessful.play();
                    }
                }, 1);

                //@K—Repeat the byte
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicKByte6.play();
                    }
                }, 3);

                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //✳️❗SO: IF THEY SUCCESSFULLY MATCH HERE GIVEN THE ARRAY TIMINGS! INCLUDE THIS FOR EACH SECTION!
                //if(THE TIMINGS MATCH){...SET kSuccessByte#=true for the current # byte to exit the loop and move on!}
                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //SO THE DEBUG CODE IS:
                //kSuccessByte#=true;

            }
        }

        if (kSuccessByte6)
        {
            //@K—PLAY ANNOUNCER SUCCESS VOICE PER-BYTE AFTER 1S DELAY
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicSuccessful.play();
                }
            }, 1);

            //@K—"Now Playing Byte 7" AFTER DELAY 4s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicNowPlayingByte7.play();
                }
            }, 4);

            //@K—ACTUALLY PLAY BYTE 7 AFTER DELAY 5s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicKByte7.play();

                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                    //✳️SO CODE GOES HERE FOR ARRAY CHECKING!
                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                }
            }, 5);
            //✳️@K—REPEAT AGAIN IF UNSUCCESSFUL!
            if(!kSuccessByte7)
            {
                //@K—play announcer "Unsuccessful" audio
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicUnsuccessful.play();
                    }
                }, 1);

                //@K—Repeat the byte
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicKByte7.play();
                    }
                }, 3);

                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //✳️❗SO: IF THEY SUCCESSFULLY MATCH HERE GIVEN THE ARRAY TIMINGS! INCLUDE THIS FOR EACH SECTION!
                //if(THE TIMINGS MATCH){...SET kSuccessByte#=true for the current # byte to exit the loop and move on!}
                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //SO THE DEBUG CODE IS:
                //kSuccessByte#=true;

            }
        }

        if (kSuccessByte7)
        {
            //@K—PLAY ANNOUNCER SUCCESS VOICE PER-BYTE AFTER 1S DELAY
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicSuccessful.play();
                }
            }, 1);

            //@K—"Now Playing Byte 8" AFTER DELAY 4s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicNowPlayingByte8.play();
                }
            }, 4);

            //@K—ACTUALLY PLAY BYTE 8 AFTER DELAY 5s
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicKByte8.play();

                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                    //✳️SO CODE GOES HERE FOR ARRAY CHECKING!
                    //✳️❗@K—SET kSuccessByte# FOR THIS PARTICULAR for() LOOP TO TRUE TO MOVE ON TO NEXT BYTE MATCHING SEQUENCE BELOW!
                }
            }, 5);
            //✳️@K—REPEAT AGAIN IF UNSUCCESSFUL!
            if(!kSuccessByte8)
            {
                //@K—play announcer "Unsuccessful" audio
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicUnsuccessful.play();
                    }
                }, 1);

                //@K—Repeat the byte
                kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                    @Override
                    public void run() {
                        musicKByte8.play();
                    }
                }, 3);

                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //✳️❗SO: IF THEY SUCCESSFULLY MATCH HERE GIVEN THE ARRAY TIMINGS! INCLUDE THIS FOR EACH SECTION!
                //if(THE TIMINGS MATCH){...SET kSuccessByte#=true for the current # byte to exit the loop and move on!}
                //✳️✳️✳️MORE CODE HERE TO BREAK THE LOOP IF THEY WERE INITIALLY UNSUCCESSFUL BUT THEN SUCCESSFULLY MATCHED—THUS BREAKING THIS LOOP✳️✳️✳️
                //SO THE DEBUG CODE IS:
                //kSuccessByte#=true;

            }
        }


        if (kSuccessByte8)
        {
            //@K—PLAY ANNOUNCER SUCCESS VOICE PER-BYTE AFTER 1S DELAY
            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {
                    musicVictory.play();
                }
            }, 1);


            kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
                @Override
                public void run() {

                    //@K—TEMP PLAYING ACTUAL SOUNDBYTE FOR DEMO PURPOSES—CHANGE TO **USER-MATCHED** SOUNDBYTE INSTEAD
                    musicKSoundByte1.play();
                }
            }, 6);



            //@K—perhaps arbitrary code here, maybe to exit the level or can simply remain onto the level until left arrow is tapped to go to Launchscreen OR escape button to quit app (already implemented as buttons)✳️
        }


        //—————//

        //✳️END MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️END MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️END MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️END MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️END MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️
        //✳️END MAIN FUNCTIONALITY FOR CODE—STILL NEEDS DEV INTEGRATION FOR TIMINGS FUNCTIONALITY AS MARKED ABOVE WITH ASTERISKS!✳️



        ///////✳️@K———❗THE DIRECTLY BELOW IS FOR DEMO PURPOSES ONLY! REMOVE ENTIRELY CODE BLOCK STARTING FROM THIS EXACT LINE ONWARDS TO WHERE IT SAYS "THE ABOVE IS FOR DEMO PURPOSES...❗———✳️/////////////
        ///////✳️@K———❗THE DIRECTLY BELOW IS FOR DEMO PURPOSES ONLY! REMOVE ENTIRELY CODE BLOCK STARTING FROM THIS EXACT LINE ONWARDS TO WHERE IT SAYS "THE ABOVE IS FOR DEMO PURPOSES...❗———✳️/////////////
        ///////✳️@K———❗THE DIRECTLY BELOW IS FOR DEMO PURPOSES ONLY! REMOVE ENTIRELY CODE BLOCK STARTING FROM THIS EXACT LINE ONWARDS TO WHERE IT SAYS "THE ABOVE IS FOR DEMO PURPOSES...❗———✳️/////////////
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicSuccessful.play();}},53);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicNowPlayingByte2.play();}},57);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicKByte2.play();}},61);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicSuccessful.play();}},70);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicNowPlayingByte3.play();}},74);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicKByte3.play();}},78);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicSuccessful.play();}},87);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicNowPlayingByte4.play();}},91);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicKByte4.play();}},95);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicSuccessful.play();}},104);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicNowPlayingByte5.play();}},108);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicKByte5.play();}},112);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicSuccessful.play();}},121);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicNowPlayingByte6.play();}},125);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicKByte6.play();}},129);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicSuccessful.play();}},138);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicNowPlayingByte7.play();}},142);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicKByte7.play();}},146);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicSuccessful.play();}},155);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicNowPlayingByte8.play();}},159);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicKByte8.play();}},163);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicVictory.play();}},172);
//        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task(){@Override public void run(){musicKSoundByte1.play();}},177);


        ///////✳️@K———❗THE DIRECTLY ABOVE IS FOR DEMO PURPOSES ONLY! REMOVE ENTIRELY CODE BLOCK STARTING FROM THIS EXACT LINE ONWARDS TO WHERE IT SAYS "THE BELOW IS FOR DEMO PURPOSES...❗———✳️/////////////
        ///////✳️@K———❗THE DIRECTLY ABOVE IS FOR DEMO PURPOSES ONLY! REMOVE ENTIRELY CODE BLOCK STARTING FROM THIS EXACT LINE ONWARDS TO WHERE IT SAYS "THE BELOW IS FOR DEMO PURPOSES...❗———✳️/////////////
        ///////✳️@K———❗THE DIRECTLY ABOVE IS FOR DEMO PURPOSES ONLY! REMOVE ENTIRELY CODE BLOCK STARTING FROM THIS EXACT LINE ONWARDS TO WHERE IT SAYS "THE BELOW IS FOR DEMO PURPOSES...❗———✳️/////////////



        BaseActor background = new BaseActor(0, 0, mainStage);
//        background.loadTexture("wallpapers/Tutorial—Reference.png");
//        background.setSize(virWidth, virHeight);
        BaseActor.setWorldBounds(background);


        //@K—instantiate and initialize "batch" var for sprite rendering (SpriteBatch rendering, that is)
        batch=new SpriteBatch();

        //Level image/sprite/texture
        levelTexture = new Texture(Gdx.files.internal("K/K_UNIVERSAL.png"),true);
        levelTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        levelSprite = new Sprite(levelTexture);
        levelSprite.setSize(kUtils.kScreenAspectRatioWidth(), kUtils.kScreenAspectRatioHeight());
//        levelSprite.setScale(0.44f);
//        levelSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/2.6),(int)(kUtils.kScreenAspectRatioHeight()/1.32));

        levelHeaderTexture = new Texture(Gdx.files.internal("K/K_DARK_THREE.png"),true);
        levelHeaderTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        levelHeaderSprite = new Sprite(levelHeaderTexture);
        levelHeaderSprite.setScale(.44f);
        levelHeaderSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/17.1),(int)(kUtils.kScreenAspectRatioHeight()/1.32));

        //Back arrow image/sprite/texture
        backArrowTexture = new Texture(Gdx.files.internal("K/K_DARK_ARROW_PRV.png"),true);
        backArrowTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        backArrowSprite = new Sprite(backArrowTexture);
        backArrowSprite.setScale(.175f);
        backArrowSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/6.15),(int)(kUtils.kScreenAspectRatioHeight()/1.14));

        //—————————————————————————————//
        //@K—INITIALIZE THE FOUR (4) SQUARE/CIRCLE TEXTURES WITH ANTIALIASING FOR SOUND-MATCHING MELODY- & ONE-SHOT-BASED EL'TS

        //@K—Render circle1 image/sprite/texture for MELODY-BASED ELEMENTS
        circleTexture1 = new Texture(Gdx.files.internal("K/K_DARK_CIRCLE.png"),true);
        circleTexture1.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        circleSprite1 = new Sprite(circleTexture1);
        circleSprite1.setScale(0.1f);
        circleSprite1.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/4.525),(int)(-kUtils.kScreenAspectRatioHeight()/2.3));

        //@K—Render circle2 image/sprite/texture for MELODY-BASED ELEMENTS
        circleTexture2 = new Texture(Gdx.files.internal("K/K_DARK_CIRCLE.png"),true);
        circleTexture2.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        circleSprite2 = new Sprite(circleTexture2);
        circleSprite2.setScale(0.1f);
        circleSprite2.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/9.5),(int)(-kUtils.kScreenAspectRatioHeight()/2.3));

        //@K—Render circle3 image/sprite/texture for MELODY-BASED ELEMENTS
        circleTexture3 = new Texture(Gdx.files.internal("K/K_DARK_CIRCLE.png"),true);
        circleTexture3.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        circleSprite3 = new Sprite(circleTexture3);
        circleSprite3.setScale(0.1f);
        circleSprite3.setPosition((int)(kUtils.kScreenAspectRatioWidth()/110),(int)(-kUtils.kScreenAspectRatioHeight()/2.3));

        //@K—Render circle4 image/sprite/texture for MELODY-BASED ELEMENTS
        circleTexture4 = new Texture(Gdx.files.internal("K/K_DARK_CIRCLE.png"),true);
        circleTexture4.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        circleSprite4 = new Sprite(circleTexture4);
        circleSprite4.setScale(0.1f);
        circleSprite4.setPosition((int)(kUtils.kScreenAspectRatioWidth()/8.15),(int)(-kUtils.kScreenAspectRatioHeight()/2.3));

        //——————————//

        //@K—Render square1 image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        squareTexture1 = new Texture(Gdx.files.internal("K/K_DARK_SQUARE.png"),true);
        squareTexture1.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        squareSprite1 = new Sprite(squareTexture1);
        squareSprite1.setScale(0.1f);
        squareSprite1.setPosition((int)(kUtils.kScreenAspectRatioWidth()/4),(int)(-kUtils.kScreenAspectRatioHeight()/2.3));

        //@K—Render square2 image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        squareTexture2 = new Texture(Gdx.files.internal("K/K_DARK_SQUARE.png"),true);
        squareTexture2.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        squareSprite2 = new Sprite(squareTexture2);
        squareSprite2.setScale(0.1f);
        squareSprite2.setPosition((int)(kUtils.kScreenAspectRatioWidth()/2.75),(int)(-kUtils.kScreenAspectRatioHeight()/2.3));

        //@K—Render square3 image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        squareTexture3 = new Texture(Gdx.files.internal("K/K_DARK_SQUARE.png"),true);
        squareTexture3.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        squareSprite3 = new Sprite(squareTexture3);
        squareSprite3.setScale(0.1f);
        squareSprite3.setPosition((int)(kUtils.kScreenAspectRatioWidth()/2.1),(int)(-kUtils.kScreenAspectRatioHeight()/2.3));

        //@K—Render square4 image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        squareTexture4 = new Texture(Gdx.files.internal("K/K_DARK_SQUARE.png"),true);
        squareTexture4.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        squareSprite4 = new Sprite(squareTexture4);
        squareSprite4.setScale(0.1f);
        squareSprite4.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.7),(int)(-kUtils.kScreenAspectRatioHeight()/2.3));

        //—————————————————————————————//




        //—————————————————————————————//
        //@K—INITIALIZE THE EIGHT (8) LETTER TEXTURES OVERLAID ON SQUARES/CIRCLES WITH ANTIALIASING (OVERLAID ON SOUND-MATCHING MELODY- & ONE-SHOT-BASED EL'TS)

        //@K—Render letter Q image/sprite/texture for MELODY-BASED ELEMENTS
        qLetterTexture = new Texture(Gdx.files.internal("K/K_DARK_Q.png"),true);
        qLetterTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        qLetterSprite = new Sprite(qLetterTexture);
        qLetterSprite.setScale(0.25f);
        qLetterSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/27.5),(int)(-kUtils.kScreenAspectRatioHeight()/29.7));

        //@K—Render letter W image/sprite/texture for MELODY-BASED ELEMENTS
        wLetterTexture = new Texture(Gdx.files.internal("K/K_DARK_W.png"),true);
        wLetterTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        wLetterSprite = new Sprite(wLetterTexture);
        wLetterSprite.setScale(0.25f);
        wLetterSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/7.1),(int)(-kUtils.kScreenAspectRatioHeight()/29.7));

        //@K—Render letter E image/sprite/texture for MELODY-BASED ELEMENTS
        eLetterTexture = new Texture(Gdx.files.internal("K/K_DARK_E.png"),true);
        eLetterTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        eLetterSprite = new Sprite(eLetterTexture);
        eLetterSprite.setScale(0.25f);
        eLetterSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/3.62),(int)(-kUtils.kScreenAspectRatioHeight()/29.7));

        //@K—Render letter R image/sprite/texture for MELODY-BASED ELEMENTS
        rLetterTexture = new Texture(Gdx.files.internal("K/K_DARK_R.png"),true);
        rLetterTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        rLetterSprite = new Sprite(rLetterTexture);
        rLetterSprite.setScale(0.25f);
        rLetterSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/2.59),(int)(-kUtils.kScreenAspectRatioHeight()/29.7));

        //——————————//

        //@K—Render letter U image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        uLetterTexture = new Texture(Gdx.files.internal("K/K_DARK_U.png"),true);
        uLetterTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        uLetterSprite = new Sprite(uLetterTexture);
        uLetterSprite.setScale(0.25f);
        uLetterSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.945),(int)(-kUtils.kScreenAspectRatioHeight()/29.7));

        //@K—Render letter I image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        iLetterTexture = new Texture(Gdx.files.internal("K/K_DARK_I.png"),true);
        iLetterTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        iLetterSprite = new Sprite(iLetterTexture);
        iLetterSprite.setScale(0.25f);
        iLetterSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.513),(int)(-kUtils.kScreenAspectRatioHeight()/29.7));

        //@K—Render letter O image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        oLetterTexture = new Texture(Gdx.files.internal("K/K_DARK_O.png"),true);
        oLetterTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        oLetterSprite = new Sprite(oLetterTexture);
        oLetterSprite.setScale(0.25f);
        oLetterSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.3625),(int)(-kUtils.kScreenAspectRatioHeight()/29.7));

        //@K—Render letter P image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        pLetterTexture = new Texture(Gdx.files.internal("K/K_DARK_P.png"),true);
        pLetterTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        pLetterSprite = new Sprite(pLetterTexture);
        pLetterSprite.setScale(0.25f);
        pLetterSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.168),(int)(-kUtils.kScreenAspectRatioHeight()/29.7));

        //—————————————————————————————//




        //Animated spectrum image/sprite/texture
        spectrumAtlas = new TextureAtlas("K/K_SPECTRUM_CUSTOM.atlas");
        spectrumRegion = spectrumAtlas.findRegion("K_SPECTRUM_CUSTOM");
        spectrumSprite=new Sprite(spectrumRegion);

        //@K—acquire imgs/frames necessary for ani—multi-frame, continuously looping render/infinitely looping animation
        //@K—*IN ORDER FOR REGIONS TO WORK AND DISPLAY ANI, FRAMES FIT ONTO >2048-SIZED SPRITESHEET; MAY CAUSE ISSUES ON MOBILE BUT WORKS PERFECTLY ON DESKTOP.
        Array<TextureAtlas.AtlasRegion> spectrumAniFrames = spectrumAtlas.findRegions("K_SPECTRUM_CUSTOM");

        //@K—actual animation creation + set loop point for continous animation
        spectrumAni = new Animation(.075f, spectrumAniFrames, Animation.PlayMode.LOOP);
//        waveformAni.setSize(kUtils.kScreenAspectRatioWidth()/2, kUtils.kScreenAspectRatioHeight()/2);
//        waveformAni.setPosition(kUtils.kScreenAspectRatioWidth()/4,-kUtils.kScreenAspectRatioHeight()/8);

        //———————————————————————————————————————————————————//
        //———————————————————————————————————————————————————//
        //———————————————————————————————————————————————————//


//        beat1 = Gdx.audio.newMusic(Gdx.files.internal("Clarence/Beat1.ogg"));
        drumPattern = Gdx.audio.newMusic(Gdx.files.internal("Clarence/drumPattern.ogg"));
        drumPattern.setLooping(false);

        soundA5 = Gdx.audio.newMusic(Gdx.files.internal("Clarence/noteA51.ogg"));
        soundA5.setLooping(false);
        // These are the other sounds, but are not yet implemented
        soundB5 = Gdx.audio.newMusic(Gdx.files.internal("Clarence/noteB51.ogg"));
        soundB5.setLooping(false);
        soundC6 = Gdx.audio.newMusic(Gdx.files.internal("Clarence/noteC61.ogg"));
        soundC6.setLooping(false);
        soundD6 = Gdx.audio.newMusic(Gdx.files.internal("Clarence/noteD61.ogg"));
        soundD6.setLooping(false);
        soundE6 = Gdx.audio.newMusic(Gdx.files.internal("Clarence/noteE61.ogg"));
        soundE6.setLooping(false);
        soundKick = Gdx.audio.newMusic(Gdx.files.internal("Clarence/kick.ogg"));
        soundKick.setLooping(false);
        soundHat = Gdx.audio.newMusic(Gdx.files.internal("Clarence/hat.ogg"));
        soundHat.setLooping(false);
        soundBass = Gdx.audio.newMusic(Gdx.files.internal("Clarence/bass.ogg"));
        soundBass.setLooping(false);
        soundSnare = Gdx.audio.newMusic(Gdx.files.internal("Clarence/snare.ogg"));
        soundSnare.setLooping(false);
        melodyPattern = Gdx.audio.newMusic(Gdx.files.internal("Clarence/melodyPattern.ogg"));
        melodyPattern.setLooping(false);



//        Timer = 0;
        TimeLeft = new Label(" ", BaseGame.labelStyle);

//        TimeLeft.setColor(Color.FOREST);
        TimeLeft.setColor(0,0,0,0f);
        TimeLeft.setPosition(20,500);
        uiStage.addActor(TimeLeft);

        beatTimeLeft= new Label(" ", BaseGame.labelStyle);

//        beatTimeLeft.setColor(Color.GOLDENROD);
        beatTimeLeft.setColor(0,0,0,0f);
        beatTimeLeft.setPosition(400,500);
        uiStage.addActor(beatTimeLeft);


        stepLabel = new Label("Step: " + step, BaseGame.labelStyle);
//        stepLabel.setColor(Color.WHITE);
        stepLabel.setColor(0,0,0,0f);
        stepLabel.setPosition(20,400);
        uiStage.addActor(stepLabel);


        ////////// BACK TO MENUSCREEN BUTTON/////////////////
/*
        Button.ButtonStyle menuButtonStyle = new Button.ButtonStyle();
        Texture menuButtonTex = new Texture(Gdx.files.internal("back1.png"));
        TextureRegion menuButtonRegion = new TextureRegion(menuButtonTex);
        menuButtonStyle.up = new TextureRegionDrawable(menuButtonRegion);
        Button menuButton = new Button(menuButtonStyle);
        menuButton.setColor(Color.WHITE);
        menuButton.setSize(100, 100);
        menuButton.setPosition(650, 50);


 */

        //@K—go to Launch✳️⬅️
        //@K—go to Launch✳️⬅️
        //@K—go to Launch✳️⬅️
        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton menuButton = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
        menuButton.setColor(0,0,0,0f);
//        menuButton.setPosition(600 * WidthOffset,450 * HeightOffset);
        menuButton.setPosition((int)(kUtils.kScreenAspectRatioWidth()/105.15),(int)(kUtils.kScreenAspectRatioHeight()/1.0625));
        menuButton.setSize((int)(kUtils.kScreenAspectRatioWidth()/12.15),(int)(kUtils.kScreenAspectRatioWidth()/70.15));
        uiStage.addActor(menuButton);
        menuButton.addListener( (Event e) -> {
            if ( !(e instanceof InputEvent) ) return false;
            if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

            //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
            Timer.instance().clear();

            SoundByte.setActiveScreen(new Launch());return true; }
        );


//
//        //// RESTART BUTTON///////////
//
//        TextButton restartButton = new TextButton( "restart", BaseGame.textButtonStyle);
//        restartButton.setColor(0,0,0,1f);
//        restartButton.setColor(Color.GREEN);
//        restartButton.setPosition(80,100);
//        //  uiStage.addActor(restartButton);
//
//        // Instructions
//        sequenceLabel = new Label("           Press U \n" +
//                " when Time = 1, 2, 4, 5, 6, 8", BaseGame.labelStyle);
//        sequenceLabel.setSize(2,2);
////        sequenceLabel.setColor(Color.CYAN);
//        sequenceLabel.setColor(0,0,0,0f);
//        sequenceLabel.setPosition(10,300);
//        uiStage.addActor(sequenceLabel);
//
//
//
//
//        menuButton.addListener(
//                (Event e) ->
//                {
//                    if ( !(e instanceof InputEvent) )
//                        return false;
//
//                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) )
//                        return false;
//
//                    SoundByte.setActiveScreen( new Launch() );
//                    return true;
//                }
//        );
//
//
//
//        //Sound Button Initialization//
//        Button.ButtonStyle SoundButtonStyle = new Button.ButtonStyle();
//        Texture SoundButtonTex = new Texture(Gdx.files.internal("Defaults/space.png"));
//        TextureRegion SoundButtonRegion = new TextureRegion(SoundButtonTex);
//        SoundButtonStyle.up = new TextureRegionDrawable(SoundButtonRegion);
//        Button SoundButton = new Button(SoundButtonStyle);
////        SoundButton.setColor(Color.WHITE);
//        SoundButton.setColor(0,0,0,0f);
//        SoundButton.setSize(500,100);
//        SoundButton.setPosition(100, 400);
//        //    uiStage.addActor(SoundButton);


        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️@K—ACTUAL SOUNDS—BOUND TO @K's MUSIC FOR NOW—same with buttons
        //@K—note: CHANGING SOUNDS HERE IS NOT THE PLACE—BELOW, WITH THE ONE-LINER IF-ELSES IS THE PLACE—THIS IS SIMPLY THE EVENT ACTIVATING
        //@K—entire section commented out.
        //////////////////////Q Button////////////////////////////////////////////////////////////////////
//        Button.ButtonStyle QButtonStyle = new Button.ButtonStyle();
//        Texture QbuttonTex = new Texture(Gdx.files.internal("Defaults/Q.png"));
//        TextureRegion QbuttonRegion = new TextureRegion(QbuttonTex);
//        QButtonStyle.up = new TextureRegionDrawable(QbuttonRegion);
//        Button QButton = new Button(QButtonStyle);
////        QButton.setColor(Color.WHITE);
//        QButton.setColor(0,0,0,0f);
//        QButton.setSize(100, 100);
//        QButton.setPosition(100, 300);
//
//
//        QButton.addListener(
//                (Event e) ->
//                {
//
//                    if (!(e instanceof InputEvent))
//                        return false;
//
//                    if (!((InputEvent) e).getType().equals(InputEvent.Type.touchDown))
//                        return false;
//
//
////                    soundA5.play();
//                    musicKByte1Sound.play();
//
//                    return true;
//                }
//        );
//
//        ////////////////W Button/////////////////////////////////////
//
//        Button.ButtonStyle WButtonStyle = new Button.ButtonStyle();
//        Texture WbuttonTex = new Texture(Gdx.files.internal("Defaults/W.png"));
//        TextureRegion WbuttonRegion = new TextureRegion(WbuttonTex);
//        WButtonStyle.up = new TextureRegionDrawable(WbuttonRegion);
//        Button WButton = new Button(WButtonStyle);
////        WButton.setColor(Color.WHITE);
//        WButton.setColor(0,0,0,0f);
//        WButton.setSize(100, 100);
//        WButton.setPosition(200, 300);
//
//
//        WButton.addListener(
//                (Event e) ->
//                {
//
//                    if (!(e instanceof InputEvent))
//                        return false;
//
//                    if (!((InputEvent) e).getType().equals(InputEvent.Type.touchDown))
//                        return false;
//
//
////                    soundB5.play();
//                    musicKByte2Sound.play();
//                    return true;
//                }
//        );
//
//        //////////E Button//////////////////////////////////
//        Button.ButtonStyle EButtonStyle = new Button.ButtonStyle();
//        Texture EbuttonTex = new Texture(Gdx.files.internal("Defaults/E.png"));
//        TextureRegion EbuttonRegion = new TextureRegion(EbuttonTex);
//        EButtonStyle.up = new TextureRegionDrawable(EbuttonRegion);
//        Button EButton = new Button(EButtonStyle);
////        EButton.setColor(Color.WHITE);
//        EButton.setColor(0,0,0,0f);
//        EButton.setSize(100, 100);
//        EButton.setPosition(300, 300);
//
//
//        EButton.addListener(
//                (Event e) ->
//                {
//
//                    if (!(e instanceof InputEvent))
//                        return false;
//
//                    if (!((InputEvent) e).getType().equals(InputEvent.Type.touchDown))
//                        return false;
//
//
////                    soundC6.play();
//                    musicKByte3Sound.play();
//                    return true;
//                }
//        );
//
//
//        ///////////////////R Button/////////////////////////////
//        Button.ButtonStyle RButtonStyle = new Button.ButtonStyle();
//        Texture RbuttonTex = new Texture(Gdx.files.internal("Defaults/R.png"));
//        TextureRegion RbuttonRegion = new TextureRegion(RbuttonTex);
//        RButtonStyle.up = new TextureRegionDrawable(RbuttonRegion);
//        Button RButton = new Button(RButtonStyle);
////        RButton.setColor(Color.WHITE);
//        RButton.setColor(0,0,0,0f);
//        RButton.setSize(100, 100);
//        RButton.setPosition(400, 300);
//
//
//        RButton.addListener(
//                (Event e) ->
//                {
//
//                    if (!(e instanceof InputEvent))
//                        return false;
//
//                    if (!((InputEvent) e).getType().equals(InputEvent.Type.touchDown))
//                        return false;
//
//
////                    soundD6.play();
//                    musicKByte4Sound.play();
//                    return true;
//                }
//        );
//
//        ///////////////////U Button/////////////////////////////
//        Button.ButtonStyle UButtonStyle = new Button.ButtonStyle();
//        Texture UbuttonTex = new Texture(Gdx.files.internal("Defaults/U.png"));
//        TextureRegion UbuttonRegion = new TextureRegion(UbuttonTex);
//        UButtonStyle.up = new TextureRegionDrawable(UbuttonRegion);
//        Button UButton = new Button(UButtonStyle);
////        UButton.setColor(Color.WHITE);
//        UButton.setColor(0,0,0,0f);
//        UButton.setSize(100, 100);
//        UButton.setPosition(100, 100);
//
//
//        UButton.addListener(
//                (Event e) ->
//                {
//
//                    if (!(e instanceof InputEvent))
//                        return false;
//
//                    if (!((InputEvent) e).getType().equals(InputEvent.Type.touchDown))
//                        return false;
//
//
////                    soundKick.play();
//                    musicKByte5Sound.play();
//                    return true;
//                }
//        );
//
//        ///////////////////I Button/////////////////////////////
//        Button.ButtonStyle IButtonStyle = new Button.ButtonStyle();
//        Texture IbuttonTex = new Texture(Gdx.files.internal("Defaults/I.png"));
//        TextureRegion IbuttonRegion = new TextureRegion(IbuttonTex);
//        IButtonStyle.up = new TextureRegionDrawable(IbuttonRegion);
//        Button IButton = new Button(IButtonStyle);
////        IButton.setColor(Color.WHITE);
//        IButton.setColor(0,0,0,0f);
//        IButton.setSize(100, 100);
//        IButton.setPosition(200, 100);
//
//
//        IButton.addListener(
//                (Event e) ->
//                {
//
//                    if (!(e instanceof InputEvent))
//                        return false;
//
//                    if (!((InputEvent) e).getType().equals(InputEvent.Type.touchDown))
//                        return false;
//
//
////                    soundSnare.play();
//                    musicKByte6Sound.play();
//                    return true;
//                }
//        );
//
//        ///////////////////O Button/////////////////////////////
//        Button.ButtonStyle OButtonStyle = new Button.ButtonStyle();
//        Texture ObuttonTex = new Texture(Gdx.files.internal("Defaults/O.png"));
//        TextureRegion ObuttonRegion = new TextureRegion(ObuttonTex);
//        OButtonStyle.up = new TextureRegionDrawable(ObuttonRegion);
//        Button OButton = new Button(OButtonStyle);
////        OButton.setColor(Color.WHITE);
//        OButton.setColor(0,0,0,0f);
//        OButton.setSize(100, 100);
//        OButton.setPosition(300, 100);
//
//
//        OButton.addListener(
//                (Event e) ->
//                {
//
//                    if (!(e instanceof InputEvent))
//                        return false;
//
//                    if (!((InputEvent) e).getType().equals(InputEvent.Type.touchDown))
//                        return false;
//
//
////                    soundBass.play();
//                    musicKByte7Sound.play();
//                    return true;
//                }
//        );
//
//        ///////////////////P Button/////////////////////////////
//        Button.ButtonStyle PButtonStyle = new Button.ButtonStyle();
//        Texture PbuttonTex = new Texture(Gdx.files.internal("Defaults/P.png"));
//        TextureRegion PbuttonRegion = new TextureRegion(PbuttonTex);
//        PButtonStyle.up = new TextureRegionDrawable(PbuttonRegion);
//        Button PButton = new Button(PButtonStyle);
////        PButton.setColor(Color.WHITE);
//        PButton.setColor(0,0,0,0f);
//        PButton.setSize(100, 100);
//        PButton.setPosition(400, 100);
//
//
//        PButton.addListener(
//                (Event e) ->
//                {
//
//                    if (!(e instanceof InputEvent))
//                        return false;
//
//                    if (!((InputEvent) e).getType().equals(InputEvent.Type.touchDown))
//                        return false;
//
//
////                    soundHat.play();
//                    musicKByte8Sound.play();
//                    return true;
//                }
//        );
        ////////////////////////ADD BUTTONS TO THE STAGE////////////////
        // uiStage.addActor(QButton);
        // uiStage.addActor(WButton);
        //   uiStage.addActor(EButton);
        //    uiStage.addActor(RButton);
        //     uiStage.addActor(UButton);
        //     uiStage.addActor(IButton);
        //     uiStage.addActor(OButton);
        //     uiStage.addActor(PButton);


        // Initial values for the variables
        playback = false;
        start = false;
        attempted = false;
        complete = false;

//        hatAdded = false;
//        BassAdded = true;


        // All times are in dt units
        // The total can be changed to make space for more sounds
//        total = 600;
        // The time interval along the total that sounds are played

        Timer1 = 0;

        counterTimer = 3;


        beatTime = 560;

        // The amount of time a key can be pressed off beat (difficulty)
        //delay = 30;
        delay1 = 1;
        // The amount of sounds
        countKick = 6;
        countSnare = 6;
        countHat = 2;
        countBass = 1;
        countMelody = 7;


        matchedSnare = new int[countSnare];
        //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
        Arrays.fill(matchedSnare, 0);

        // This array makes sure that a button isn't pressed more than it is supposed to be during it's interval
        // i.e. if it is pressed more than once, this will count that whereas simple T/F booleans could not
        matchedKick = new int[countKick];
        //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
        Arrays.fill(matchedKick, 0);

        matchedHat = new int[countHat];
        //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
        Arrays.fill(matchedHat, 0);

        matchedBass = new int[countBass];
        //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
        Arrays.fill(matchedBass, 0);

        matchedMelody = new int[countMelody];
        //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
        Arrays.fill(matchedMelody, 0);


        // Instructions

        // The onscreen instructions
        //@K—made Label "instructions" local method var since only utilized in here/this initialize() method
        Label instructions = new Label("Press M to begin", BaseGame.labelStyle);
        instructions.setPosition(0,0);
//        instructions.setColor(Color.WHITE);
        instructions.setColor(0,0,0,0f);
        uiStage.addActor(instructions);

        /*
        TextButton helpButton = new TextButton( "Help", BaseGame.textButtonStyle );
        helpButton.setPosition(600,200);
        helpButton.setColor(Color.BLACK);
        uiStage.addActor(helpButton);

        helpButton.addListener(
                (Event e) ->
                {
                    if ( !(e instanceof InputEvent) )
                        return false;

                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) )
                        return false;

                    SoundByte.setActiveScreen( new Help() );
                    return true;
                }
        );


         */



/*

        restartButton.addListener(
                (Event e) ->
                {
                    if ( !(e instanceof InputEvent) )
                        return false;

                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) )
                        return false;

                    return true;
                }
        );

 */


        //✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️
        //✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️
        //✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️✳️ ✅

        //—————————————@K—ACTUAL INVIS BUTTONS, NOT ABOVE—these actors are added to the stage——————————————//
        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        //✳️✳️✳️✳️@K—BOUND TO @K'S MUSIC FOR NOW
        TextButton circle1Button = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
//        circle1Button.setColor(1,1,1,1f);
        circle1Button.setColor(0,0,0,0f);
//        square1Button.setPosition(600 * WidthOffset,450 * HeightOffset);
        circle1Button.setPosition((int)(kUtils.kScreenAspectRatioWidth()/15.75),(int)(kUtils.kScreenAspectRatioHeight()/14.5));
        circle1Button.setSize((int)(kUtils.kScreenAspectRatioWidth()/15.5),(int)(kUtils.kScreenAspectRatioWidth()/15.5));
        uiStage.addActor(circle1Button);
        circle1Button.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

//            soundA5.play(); if(soundA5.isPlaying()){soundA5.stop();soundA5.play();}
                    musicKByte1Sound.play(); if(musicKByte1Sound.isPlaying()){musicKByte1Sound.stop();musicKByte1Sound.play();}

                    return true; }
        );

        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton circle2Button = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
//        circle2Button.setColor(1,1,1,1f);
        circle2Button.setColor(0,0,0,0f);
//        square1Button.setPosition(600 * WidthOffset,450 * HeightOffset);
        circle2Button.setPosition((int)(kUtils.kScreenAspectRatioWidth()/5.6),(int)(kUtils.kScreenAspectRatioHeight()/14.5));
        circle2Button.setSize((int)(kUtils.kScreenAspectRatioWidth()/15.5),(int)(kUtils.kScreenAspectRatioWidth()/15.5));
        uiStage.addActor(circle2Button);
        circle2Button.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

//            soundB5.play(); if(soundB5.isPlaying()){soundB5.stop();soundB5.play();}
                    musicKByte2Sound.play(); if(musicKByte2Sound.isPlaying()){musicKByte2Sound.stop();musicKByte2Sound.play();}

                    return true; }
        );

        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton circle3Button = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
//        circle3Button.setColor(1,1,1,1f);
        circle3Button.setColor(0,0,0,0f);
//        square1Button.setPosition(600 * WidthOffset,450 * HeightOffset);
        circle3Button.setPosition((int)(kUtils.kScreenAspectRatioWidth()/3.42),(int)(kUtils.kScreenAspectRatioHeight()/14.5));
        circle3Button.setSize((int)(kUtils.kScreenAspectRatioWidth()/15.5),(int)(kUtils.kScreenAspectRatioWidth()/15.5));
        uiStage.addActor(circle3Button);
        circle3Button.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

//            soundC6.play(); if(soundC6.isPlaying()){soundC6.stop();soundC6.play();}
                    musicKByte3Sound.play(); if(musicKByte3Sound.isPlaying()){musicKByte3Sound.stop();musicKByte3Sound.play();}
                    return true; }
        );

        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton circle4Button = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
//        circle4Button.setColor(1,1,1,1f);
        circle4Button.setColor(0,0,0,0f);
//        square1Button.setPosition(600 * WidthOffset,450 * HeightOffset);
        circle4Button.setPosition((int)(kUtils.kScreenAspectRatioWidth()/2.46),(int)(kUtils.kScreenAspectRatioHeight()/14.5));
        circle4Button.setSize((int)(kUtils.kScreenAspectRatioWidth()/15.5),(int)(kUtils.kScreenAspectRatioWidth()/15.5));
        uiStage.addActor(circle4Button);
        circle4Button.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

//            soundE6.play(); if(soundE6.isPlaying()){soundE6.stop();soundE6.play();}
                    musicKByte4Sound.play(); if(musicKByte4Sound.isPlaying()){musicKByte4Sound.stop();musicKByte4Sound.play();}

                    return true; }
        );

        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton square1Button = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
//        square1Button.setColor(1,1,1,1f);
        square1Button.setColor(0,0,0,0f);
//        square1Button.setPosition(600 * WidthOffset,450 * HeightOffset);
        square1Button.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.8725),(int)(kUtils.kScreenAspectRatioHeight()/14.5));
        square1Button.setSize((int)(kUtils.kScreenAspectRatioWidth()/15.5),(int)(kUtils.kScreenAspectRatioWidth()/15.5));
        uiStage.addActor(square1Button);
        square1Button.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

//            soundKick.play(); if(soundKick.isPlaying()){soundKick.stop();soundKick.play();}
                    musicKByte5Sound.play(); if(musicKByte5Sound.isPlaying()){musicKByte5Sound.stop();musicKByte5Sound.play();}
                    return true; }
        );

        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton square2Button = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
//        square2Button.setColor(1,1,1,1f);
        square2Button.setColor(0,0,0,0f);
//        square1Button.setPosition(600 * WidthOffset,450 * HeightOffset);
        square2Button.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.545),(int)(kUtils.kScreenAspectRatioHeight()/14.5));
        square2Button.setSize((int)(kUtils.kScreenAspectRatioWidth()/15.5),(int)(kUtils.kScreenAspectRatioWidth()/15.5));
        uiStage.addActor(square2Button);
        square2Button.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

//            soundSnare.play(); if(soundSnare.isPlaying()){soundSnare.stop();soundSnare.play();}
                    musicKByte6Sound.play(); if(musicKByte6Sound.isPlaying()){musicKByte6Sound.stop();musicKByte6Sound.play();}
                    return true; }
        );

        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton square3Button = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
//        square3Button.setColor(1,1,1,1f);
        square3Button.setColor(0,0,0,0f);
//        square1Button.setPosition(600 * WidthOffset,450 * HeightOffset);
        square3Button.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.315),(int)(kUtils.kScreenAspectRatioHeight()/14.5));
        square3Button.setSize((int)(kUtils.kScreenAspectRatioWidth()/15.5),(int)(kUtils.kScreenAspectRatioWidth()/15.5));
        uiStage.addActor(square3Button);
        square3Button.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

//            soundBass.play(); if(soundBass.isPlaying()){soundBass.stop();soundBass.play();}
                    musicKByte7Sound.play(); if(musicKByte7Sound.isPlaying()){musicKByte7Sound.stop();musicKByte7Sound.play();}
                    return true; }
        );

        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton square4Button = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
//        square4Button.setColor(1,1,1,1f);
        square4Button.setColor(0,0,0,0f);
//        square1Button.setPosition(600 * WidthOffset,450 * HeightOffset);
        square4Button.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.1467),(int)(kUtils.kScreenAspectRatioHeight()/14.5));
        square4Button.setSize((int)(kUtils.kScreenAspectRatioWidth()/15.5),(int)(kUtils.kScreenAspectRatioWidth()/15.5));
        uiStage.addActor(square4Button);
        square4Button.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

//            soundHat.play(); if(soundHat.isPlaying()){soundHat.stop();soundHat.play();}
                    musicKByte8Sound.play(); if(musicKByte8Sound.isPlaying()){musicKByte8Sound.stop();musicKByte8Sound.play();}
                    return true; }
        );

        //————————————————————————//
        //————————————————————————//
        //————————————————————————//
        //————————————————————————//
        // ————————————————————————//
        //————————————————————————//
        //————————————————————————//
        //————————————————————————//
        //————————————————————————//
        //————————————————————————//
        //————————————————————————//
        //————————————————————————//
        // ————————————————————————//
        //————————————————————————//
        //————————————————————————//
        //————————————————————————//


    }

    public void update(float dt) {
        //COMMENT THIS VARIABLE LATER
        //step = 3;

//        if(kTimer!=null){kTimer.stop();kTimer.clear();}

        //@K—immediately stop playing in the edge case of the 5s delay with the runnable still playing in another screen from Launch.java
        //... as we don't want the ambience music in the tutorial OR the actual levels OR the Experiment mode
        //... and the apploop is always running and update is always activating, so this will cause the sound to stop while in the level
        //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
        if(musicIntroAmbience!=null) musicIntroAmbience.stop();

        //@K—stop all sounds as part of the actual patterns/bytes
//        if(soundA5!=null) soundA5.stop();
//        if(soundB5!=null) soundB5.stop();
//        if(soundC6!=null) soundC6.stop();
//        if(soundE6!=null) soundE6.stop();
//        if(soundKick!=null) soundKick.stop();
//        if(soundSnare!=null) soundSnare.stop();
//        if(soundBass!=null) {soundBass.stop();}
//        if(soundHat!=null) soundHat.stop();


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—stop all sounds as part of the actual patterns/bytes—MY/K's MUSIC
//        if (musicKSoundByte1 != null) musicKSoundByte1.stop();
//        if (musicKByte1 != null) musicKByte1.stop();
//        if (musicKByte2 != null) musicKByte2.stop();
//        if (musicKByte3 != null) musicKByte3.stop();
//        if (musicKByte4 != null) musicKByte4.stop();
//        if (musicKByte5 != null) musicKByte5.stop();
//        if (musicKByte6 != null) musicKByte6.stop();
//        if (musicKByte7 != null) musicKByte7.stop();
//        if (musicKByte8 != null) musicKByte8.stop();
//        if (musicKByte2Sound != null) musicKByte2Sound.stop();
//        if (musicKByte3Sound != null) musicKByte3Sound.stop();
//        if (musicKByte1Sound != null) musicKByte1Sound.stop();
//        if (musicKByte4Sound != null) musicKByte4Sound.stop();
//        if (musicKByte5Sound != null) musicKByte5Sound.stop();
//        if (musicKByte6Sound != null) musicKByte6Sound.stop();
//        if (musicKByte7Sound != null) musicKByte7Sound.stop();
//        if (musicKByte8Sound != null) musicKByte8Sound.stop();



        //@K—STOP ANY ANNOUNCER DIALOGUE + BEAT AUDIO ACROSS LEVELS/EXPERIMENT/TUTORIAL——————//

//
//        if (musicNowPlayingByte1 != null) musicNowPlayingByte1.stop();
//        if (musicNowPlayingByte2 != null) musicNowPlayingByte2.stop();
//        if (musicNowPlayingByte3 != null) musicNowPlayingByte3.stop();
//        if (musicNowPlayingByte4 != null) musicNowPlayingByte4.stop();
//        if (musicNowPlayingByte5 != null) musicNowPlayingByte5.stop();
//        if (musicNowPlayingByte6 != null) musicNowPlayingByte6.stop();
//        if (musicNowPlayingByte7 != null) musicNowPlayingByte7.stop();
//        if (musicNowPlayingByte8 != null) musicNowPlayingByte8.stop();
//        if (music3AndAHalfMinutes != null) music3AndAHalfMinutes.stop();
//        if (music5Minutes != null) music5Minutes.stop();
//        if (music7Minutes != null) music7Minutes.stop();
//        if (musicCountdown != null) musicCountdown.stop();
        if (musicExperiment != null) musicExperiment.stop();
        if (musicSoundset != null) musicSoundset.stop();
        if (musicTutorial != null) musicTutorial.stop();
//        if (musicUnsuccessful != null) musicUnsuccessful.stop();
//        if (musicSuccessful != null) musicSuccessful.stop();
//        if (musicVictory != null) musicVictory.stop();



        //@K—Render Level image/sprite/texture
        batch.begin();
        levelSprite.draw(batch);
        batch.end();

        //@K—Render Level header image/sprite/texture
        batch.begin();
        levelHeaderSprite.draw(batch);
        batch.end();

        //——————————————//

        //@K—Render circle 1 image/sprite/texture for MELODY-BASED ELEMENTS
        batch.begin();
        circleSprite1.draw(batch);
        batch.end();

        //@K—Render circle 2 image/sprite/texture for MELODY-BASED ELEMENTS
        batch.begin();
        circleSprite2.draw(batch);
        batch.end();

        //@K—Render circle 3 image/sprite/texture for MELODY-BASED ELEMENTS
        batch.begin();
        circleSprite3.draw(batch);
        batch.end();

        //@K—Render circle 4 image/sprite/texture for MELODY-BASED ELEMENTS
        batch.begin();
        circleSprite4.draw(batch);
        batch.end();

        //————//

        //@K—Render square 1 image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        batch.begin();
        squareSprite1.draw(batch);
        batch.end();

        //@K—Render square 2 image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        batch.begin();
        squareSprite2.draw(batch);
        batch.end();

        //@K—Render square 3 image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        batch.begin();
        squareSprite3.draw(batch);
        batch.end();

        //@K—Render square 4 image/sprite/texture for ONE-SHOT-BASED ELEMENTS
        batch.begin();
        squareSprite4.draw(batch);
        batch.end();

        //——————————————————//




        //——————————————//


        //✳️✳️
        //✳️✳️
        //✳️✳️
        //✳️✳️
        //✳️✳️
        //✳️✳️
        //✳️✳️
        //✳️✳️
        //✳️✳️
        //✳️✳️
        //✳️✳️
        //✳️✳️
        //@K—HIDE THESE LETTERS IF NEED BE TO MAKE THEM APPEAR ONLY WHEN NEEDED IN TUTORIAL

        //@K—BATCH RENDER THE EIGHT (8) LETTER TEXTURES OVERLAID ON SQUARES/CIRCLES WITH ANTIALIASING (OVERLAID ON SOUND-MATCHING MELODY- & ONE-SHOT-BASED EL'TS)
        batch.begin();
        qLetterSprite.draw(batch);
        batch.end();

        batch.begin();
        wLetterSprite.draw(batch);
        batch.end();

        batch.begin();
        eLetterSprite.draw(batch);
        batch.end();

        batch.begin();
        rLetterSprite.draw(batch);
        batch.end();

        //————//

        batch.begin();
        uLetterSprite.draw(batch);
        batch.end();

        batch.begin();
        iLetterSprite.draw(batch);
        batch.end();

        batch.begin();
        oLetterSprite.draw(batch);
        batch.end();

        batch.begin();
        pLetterSprite.draw(batch);
        batch.end();

        //————————————//



        //@K—Render back arrow image/sprite/texture
        batch.begin();
        backArrowSprite.draw(batch);
        batch.end();

        //———————————————————————————————————————————————————//

        //@K—animations initiation ...
        timePassed += Gdx.graphics.getDeltaTime();
//
//        //@K—acquire current frame to render at this specific instant for waveform ani texture
//        waveformCurrentFrame = (TextureRegion) waveformAni.getKeyFrame(timePassed);
//
//        //@K—actually render the waveform ani! :)
//        batch.begin();
//        batch.draw(waveformCurrentFrame,(int) (kUtils.kScreenAspectRatioWidth()/3.7),(int) (-kUtils.kScreenAspectRatioHeight()/9.25));
//        batch.end();
//
//        //———————————————————————————————————————————————————//
//
//        if(waveformClickedStatus)
//        {
//            //@K—timed passed regarding frames—begin counting once clicked so as to begin at frame 0
//            waveformTimePassed += Gdx.graphics.getDeltaTime();
//
//            //@K—RESET FRAME TO 0 SO ANI CAN PLAY at frame 1
//            //@K—acquire current frame to render at this specific instant for spectrum ani texture
//            waveformClickedCurrentFrame = (TextureRegion) waveformClickedAni.getKeyFrame(waveformTimePassed);
//
//            //@K—actually render the CLICKED waveform ani! :)
//            batch.begin();
//            batch.draw(waveformClickedCurrentFrame,(int) (kUtils.kScreenAspectRatioWidth()/3.7),(int) (-kUtils.kScreenAspectRatioHeight()/9.25));
//            batch.end();
//        }

        //———————————————————————————————————————————————————//

        //@K—acquire current frame to render at this specific instant for spectrum ani texture
        spectrumCurrentFrame = (TextureRegion) spectrumAni.getKeyFrame(timePassed);

        //@K—actually render the spectrum ani! :)
        batch.begin();
        batch.draw(spectrumCurrentFrame,(int) (kUtils.kScreenAspectRatioWidth()/6.8),(int) (kUtils.kScreenAspectRatioHeight()/4.5));
        batch.end();

        //———————————————————————————————————————————————————//




        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        //✳️✳️✳️✳️
        ////✳️✳️✳️✳️@K—ACTUAL KEYBOARD BINDINGS


        // Plays the Q sound
//        if (Gdx.input.isKeyPressed(Input.Keys.Q))
        if(Gdx.input.isKeyJustPressed(Input.Keys.Q))
//        {soundA5.play(); if(soundA5.isPlaying()){soundA5.stop();soundA5.play();}}
        {musicKByte1Sound.play(); if(musicKByte1Sound.isPlaying()){musicKByte1Sound.stop();musicKByte1Sound.play();}}

//        if (Gdx.input.isKeyPressed(Input.Keys.W))
        if (Gdx.input.isKeyJustPressed(Input.Keys.W))
//        {soundB5.play(); if(soundB5.isPlaying()){soundB5.stop();soundB5.play();}}
        {musicKByte2Sound.play(); if(musicKByte2Sound.isPlaying()){musicKByte2Sound.stop();musicKByte2Sound.play();}}

//        if (Gdx.input.isKeyPressed(Input.Keys.E))
        if(Gdx.input.isKeyJustPressed(Input.Keys.E))
//        {soundC6.play(); if(soundC6.isPlaying()){soundC6.stop();soundC6.play();}}
        {musicKByte3Sound.play(); if(musicKByte3Sound.isPlaying()){musicKByte3Sound.stop();musicKByte3Sound.play();}}

//        if (Gdx.input.isKeyPressed(Input.Keys.R))
        if(Gdx.input.isKeyJustPressed(Input.Keys.R))
//        {soundE6.play(); if(soundE6.isPlaying()){soundE6.stop();soundE6.play();}}
        {musicKByte4Sound.play(); if(musicKByte4Sound.isPlaying()){musicKByte4Sound.stop();musicKByte4Sound.play();}}


//        if (Gdx.input.isKeyPressed(Input.Keys.U))
        if(Gdx.input.isKeyJustPressed(Input.Keys.U))
//        {soundKick.play(); if(soundKick.isPlaying()){soundKick.stop();soundKick.play();}}
        {musicKByte5Sound.play(); if(musicKByte5Sound.isPlaying()){musicKByte5Sound.stop();musicKByte5Sound.play();}}

//        if (Gdx.input.isKeyPressed(Input.Keys.I))
        if(Gdx.input.isKeyJustPressed(Input.Keys.I))
//        {soundSnare.play(); if(soundSnare.isPlaying()){soundSnare.stop();soundSnare.play();}}
        {musicKByte6Sound.play(); if(musicKByte6Sound.isPlaying()){musicKByte6Sound.stop();musicKByte6Sound.play();}}


//        if (Gdx.input.isKeyPressed(Input.Keys.O))
        if(Gdx.input.isKeyJustPressed(Input.Keys.O))
//        {soundBass.play(); if(soundBass.isPlaying()){soundBass.stop();soundBass.play();}}
        {musicKByte7Sound.play(); if(musicKByte7Sound.isPlaying()){musicKByte7Sound.stop();musicKByte7Sound.play();}}

//        if (Gdx.input.isKeyPressed(Input.Keys.P))
        if(Gdx.input.isKeyJustPressed(Input.Keys.P))
//        {soundHat.play(); if(soundHat.isPlaying()){soundHat.stop();soundHat.play();}}
        {musicKByte8Sound.play(); if(musicKByte8Sound.isPlaying()){musicKByte8Sound.stop();musicKByte8Sound.play();}}



        // Starts the playback
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            playback = true;

            if(step == 0 || step == 1 || step == 2)
            {
                drumPatternPlay =true;
            }



            // kickPatternPlay = true;
            // snarePatternPlay = true;
            //hatandbassPatternPLay = true;

            if(step == 3) {
                melodyPatternPlay = true;
            }
        }

        // Stops the playback (Currently has no picture)
        if (Gdx.input.isKeyPressed(Input.Keys.N)) {
            playback = false;
        }

        // Starts tracking your key preses to match (Currently no picture)
        if (Gdx.input.isKeyPressed(Input.Keys.M)) {


            counterStart = true;

            // start = true;


        }
        if(Gdx.input.isKeyPressed(Input.Keys.H)){
            Timer1 = 0;
            counterTimer = 3;
            start = false;
            attempted = false;
            counterStart = true;
            countKick = 6;
            countSnare = 6;
            countHat = 2;
            countBass = 1;
            correctMelody = 7;
            correctKick = 0;
            correctSnare = 0;
            correctHat = 0;
            correctBass = 0;
            correctMelody =0;
            beatTime = 560;

            ////////////RESETS KICK///////
            matchedKick = new int[countKick];

            //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
            Arrays.fill(matchedKick, 0);

            ////////////RESETS SNARE///////
            matchedSnare = new int[countSnare];

            //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
            Arrays.fill(matchedSnare, 0);

            ////////////RESETS HAT///////
            matchedHat = new int[countHat];

            //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
            Arrays.fill(matchedHat, 0);

            ////////////RESETS BASS///////
            matchedBass = new int[countBass];

            //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
            Arrays.fill(matchedBass, 0);


            ////////////RESETS MELODY///////
            matchedMelody = new int[countMelody];

            //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
            Arrays.fill(matchedMelody, 0);

            soundHat.stop();
            soundBass.stop();

        }



        if(counterStart && counterTimer>=0)
        {
            counterTimer -= dt;
            TimeLeft.setText("Countdown: " + Math.round(counterTimer));
            TimeLeft.setColor(0,0,0,0f);


            if(counterTimer <= 0) {
                beatTime = 560;

                //playback = true;
                start = true;
            }
        }


        {

/*
            // Playback the sound to match
            if (playback) {
                TimeLeft.setText("Time Left: " + Math.round(Timer));

                if (Timer >= 0) {
                    Timer += dt;

                }
                beatTime -= dt;


                // Updates timer on every dt
                //  beatTime -= dt;

                // timerLabel.setText(Math.abs(beatTime));
                //  timerLabel.setText("Time: " +beatTime);


                //if (Timer >= 1.0000 && Timer <=1.1000)
                if (beatTime == 500) {

                    soundKick.play();

                }
                if (beatTime == 407)
                //  if (Timer >= 2.5600 && Timer <=2.6600)
                {

                    soundKick.play();
                }
                if (beatTime == 281)
                // if (Timer >= 4.6500 && Timer <=4.7500)
                {

                    soundKick.play();
                }

                // if (Timer >= 5.1700 && Timer <=5.2700)
                if (beatTime == 250) {

                    soundKick.play();
                }
                //if (Timer >= 6.7300 && Timer <=6.8300)
                if (beatTime == 157) {

                    soundKick.play();
                }
                //if (Timer >= 8.8200 && Timer <=8.9200)
                if (beatTime == 31) {

                    soundKick.play();
                }

/////////////HAT AND BASS///////////////////
                // if (beatTime == 300)
                if (beatTime == 500)
                //if(Timer >= 1.0000 && Timer <=1.1000)
                {

                    soundHat.play();
                    soundHat.setLooping(true);

                }
                // if (beatTime == 200)
                if (beatTime == 500)
                //if(Timer >= 1.0000 && Timer <=1.1000)
                {

                    soundBass.play();
                    soundBass.setLooping(true);
                }

///////////////SNARE/////////////
                if (beatTime == 438)
                // if(Timer >= 2.0400 && Timer <=2.1400)
                {

                    soundSnare.play();
                }
                if (beatTime == 313)
                //if(Timer >= 4.1300&& Timer <=4.2300)
                {

                    soundSnare.play();
                }
                if (beatTime == 266)
                //if(Timer >= 4.9100 && Timer <=5.0100)
                {

                    soundSnare.play();
                }
                if (beatTime == 188)
                //if(Timer >= 6.2100 && Timer <=6.3100)
                {

                    soundSnare.play();
                }
                if (beatTime == 62)
                //if(Timer >= 8.300 && Timer <=8.4000)
                {

                    soundSnare.play();
                }
                if (beatTime == 16)
                //if(Timer >= 9.0800 && Timer <=9.1800)
                {

                    soundSnare.play();
                }

                ////////////////MELODY////////////////
                if (beatTime == 500)
                // if (Timer>=1.00000 && Timer <= 2.00000)
                {
                    // Plays the sound for the specified count amount of times
                    soundA5.play();
                }
                if (beatTime == 469)
                // if (Timer >=1.5200 && Timer <=1.6200)
                {
                    // Plays the sound for the specified count amount of times
                    soundE6.play();
                }
                if (beatTime == 438)
                // if (Timer >=2.0400 && Timer <=2.1400)
                {
                    // Plays the sound for the specified count amount of times
                    soundC6.play();
                }

                if (beatTime == 407)
                // if (Timer >=2.56 && Timer <=2.66)
                {
                    // Plays the sound for the specified count amount of times
                    soundB5.play();
                }
                if (beatTime == 344)
                // if (Timer >=3.60 && Timer <=3.70)
                {
                    // Plays the sound for the specified count amount of times
                    soundE6.play();
                }
                if (beatTime == 313)
                // if (Timer >=4.13 && Timer <=4.23)
                {
                    // Plays the sound for the specified count amount of times
                    soundC6.play();
                }

                if (beatTime == 281)
                //if (Timer >=4.65 && Timer <= 4.75)
                {
                    // Plays the sound for the specified count amount of times
                    soundB5.play();
                }


                if (beatTime == 250)
                // if (Timer>=1.00000 && Timer <= 2.00000)
                {
                    // Plays the sound for the specified count amount of times
                    soundA5.play();
                }
                if (beatTime == 219)
                // if (Timer >=1.5200 && Timer <=1.6200)
                {

                    soundE6.play();
                }
                if (beatTime == 188)
                // if (Timer >=2.0400 && Timer <=2.1400)
                {
                    // Plays the sound for the specified count amount of times
                    soundC6.play();
                }

                if (beatTime == 157)
                // if (Timer >=2.56 && Timer <=2.66)
                {
                    // Plays the sound for the specified count amount of times
                    soundB5.play();
                }
                if (beatTime == 94)
                // if (Timer >=3.60 && Timer <=3.70)
                {
                    // Plays the sound for the specified count amount of times
                    soundE6.play();
                }
                if (beatTime == 62)
                // if (Timer >=4.13 && Timer <=4.23)
                {
                    // Plays the sound for the specified count amount of times
                    soundC6.play();
                }

                if (beatTime == 31)
                //if (Timer >=4.65 && Timer <= 4.75)
                {
                    // Plays the sound for the specified count amount of times
                    soundB5.play();
                }


                if (beatTime <= 0)
                //if(Timer >= 9.3400 && Timer <=9.4400)
                {
                    beatTime = 501;

                    //beatTime = 501;
                    playback = true;

                    countKick = 6;
                    //countSnare = 6;

                    ///SET TIMER TO 1 FOR SMOOTH LOOPING
                    //Timer = 1;


                }


            }

 */
            //@K—removed "==true" as redundant—Nov 12, 2020 at 7.31PM
            if (playback) {
                if (Gdx.input.isKeyPressed(Input.Keys.V)) {
                    playback = false;
                    melodyPatternPlay = false;
//                    kickPatternPlay = false;
//                    snarePatternPlay = false;
//                    hatandbassPatternPLay = false;
                    melodyPattern.stop();

                    drumPatternPlay = false;

                    drumPattern.stop();

                    soundBass.stop();
                    soundHat.stop();
//                    Timer = 0;
                    countKick = 6;
                    countSnare = 6;
                }
            }


        }

        //@K—made Label "incorrectLabel" local method var since only utilized in here/this initialize() method
        //Incorrect Label appears if input notes are not equal to given notes
        Label incorrectLabel;
        if(step == 0)
        {

            //@K—removed "==true" as redundant—Nov 12, 2020 at 7.31PM
            if(drumPatternPlay)
            {
                drumPattern.play();
            }


            stepLabel.setText("Step 1: Make Kick Pattern");
            stepLabel.setColor(0,0,0,0f);
            beatTimeLeft.setText(beatTime);

            // Try to match the playback
            if (start) {


                drumPatternPlay = false;
                drumPattern.stop();

                Timer1 += dt;
                beatTime-=dt;


                TimeLeft.setText("Time : " + Math.round(Timer1));


                //////// KICK //////////////

                if (Gdx.input.isKeyJustPressed(Input.Keys.U)) {

                    if(beatTime>500 && beatTime<500+delay)
                    // if (Timer1>=1 - delay1 &&Timer1 <= 1+delay1)
                    // First beat

                    // if (Timer1 > 1.0000 - delay1 && Timer1 < 1.0000 + delay1)


                    {

                        matchedKick[0]++;
                    }

                    if(beatTime>440-delay && beatTime<440+delay)

                    //if (Timer1 > 2 - delay1 && Timer1 <= 2)


                    // if (Timer1 >= 1.5600 - delay1 && Timer1 <= 1.6600 + delay1)

                    {

                        matchedKick[1]++;
                    }


                    if(beatTime>320 && beatTime<320+delay)
                    //if (Timer1 > 4 - delay1 && Timer1 <= 4+delay1)
                    // if (Timer1 >= 3.6500 - delay1 && Timer1 <= 3.7500 + delay1)

                    {
                        matchedKick[2]++;
                    }

                    if(beatTime>260 && beatTime<260+delay)
                    //if (Timer1 > 5 - delay1 && Timer1 <= 5+delay1)
                    //if (Timer1 >= 4.1700 - delay1 && Timer1 <= 4.1800 + delay1)
                    {
                        matchedKick[3]++;
                    }

                    if(beatTime>200-delay && beatTime<200+delay)
                    //if (Timer1 > 6 - delay1 && Timer1 <= 6+delay1)
                    //if (Timer1 >= 5.7300 -delay1 && Timer1 <= 5.8300 +delay1)
                    {
                        matchedKick[4]++;
                    }

                    if(beatTime>80-delay && beatTime<80+delay)
                    //if (Timer1 > 8 - delay1 && Timer1 <= 8+delay1)
                    //if (Timer1 >= 7.8200 -delay1 && Timer1 <= 7.9200 +delay1)

                    {
                        matchedKick[5]++;
                    }


                }

                // time runs out, stop listening for keys
                //if (total == 0)
                if (Timer1 >= 9.34 && Timer1 <= 10) {
                    //  if (Timer1 >= 9.3400 && Timer1 <= 9.4400) {
                    attempted = true;
                    start = false;
                    Timer1 = 0;
                    beatTime = 560;
                    // total = 500;
                }

            }


            // Checks if the attempt was correct
            if (attempted) {

                counterTimer = 3;
                counterStart = false;
                // Checks if the correct keys were pressed the correct number of times in each interval
                //@K—simpler "enhanced for loop" methodology—Nov 12, 2020 at 7.32PM
                for (int j : matchedKick) {
                    if (j == 1) {
                        correctKick++;
                    }
                }
                // If the sound was matched, the level is complete

                /// CHANGE TO correctSnare = countSnare in SNARE section
                if (correctKick == countKick) {
                    complete = true;


                }
                // Otherwise, reset the matched array for the next attempt
                else {
                    incorrectLabel = new Label("\n Kicks Matched: " + correctKick +
                            "\n Total Kicks: "+ countKick, BaseGame.labelStyle);

//                    incorrectLabel.setColor(Color.RED);
                    incorrectLabel.setColor(0,0,0,0f);
                    uiStage.addActor(incorrectLabel);
                    incorrectLabel.setPosition(100, 100);
                    incorrectLabel.addAction(Actions.delay(1));
                    incorrectLabel.addAction(Actions.after(Actions.fadeOut(4)));
                    //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
                    Arrays.fill(matchedKick, 0);
                    correctKick = 0;


                    attempted = false;
                }
            }

            // When the level is complete, returns goes to another level
            if (complete) {

                countKick = 6;
                countSnare = 6;
                countHat = 2;
                countBass = 1;
                correctMelody = 7;
                attempted = false;
                complete = false;
                correctKick = 0;
                correctSnare = 0;
                correctHat = 0;
                correctBass = 0;
                correctMelody =0;
                step = 1;

                //SoundByte.setActiveScreen(new completeScreen());
            }
        }



        if(step ==1){

            //@K—removed "==true" as redundant—Nov 12, 2020 at 7.31PM
            if(drumPatternPlay)
            {
                drumPattern.play();
            }

            sequenceLabel.setText("Press I \n when Time = 2, 4, 5, 6, 8, 9");
            beatTimeLeft.setText(beatTime);



            stepLabel.setText("Step 2: Make Snare Pattern");

            // Playback the sound to match

            // Try to match the playback
            if (start) {

                drumPatternPlay = false;
                drumPattern.stop();

                beatTime-=dt;

                Timer1 += dt;

                TimeLeft.setText("Time : " + Math.round(Timer1));


                ///////////SNARE//////////////

                if (Gdx.input.isKeyJustPressed(Input.Keys.I)) {

                    ///KEY SHOULD BE PRESSED on <=x number

                    if(beatTime>=440-delay && beatTime<=440+delay)
                    //if (Timer1 >= 2-delay1 && Timer1 <= 2+delay1)
                    //if (Timer1 >= 2.0400 -delay1 && Timer1 <= 2.1400 +delay1)
                    {
                        matchedSnare[0]++;

                    }

                    if(beatTime>=320 && beatTime<=320+delay)
                    //if (Timer1 >= 4-delay1 && Timer1 <= 4.000+delay1)
                    // if (Timer1 >= 4.1300-delay1 && Timer1 <= 4.2300 +delay1)
                    {
                        matchedSnare[1]++;

                    }

                    if(beatTime>=260 && beatTime<=260+delay)
                    //if (Timer1 >= 5-delay1  && Timer1 <= 5+delay1)
                    //if (Timer1 >= 4.9100 -delay1 && Timer1 <= 5.0100 +delay1)
                    {
                        matchedSnare[2]++;

                    }

                    if(beatTime>=200-delay && beatTime<=200+delay)
                    //if (Timer1 >= 6-delay1 && Timer1 <= 6+delay1)
                    //if (Timer1 >= 6.2100 -delay1 && Timer1 <= 6.3100 +delay1)
                    {
                        matchedSnare[3]++;

                    }

                    if(beatTime>=80 && beatTime<=80+delay)
                    //if (Timer1 >= 8-delay1 && Timer1 <= 8+delay1)
                    //if (Timer1 >= 8.3000 -delay1 && Timer1 <= 8.4000 +delay1)
                    {
                        matchedSnare[4]++;

                    }

                    if(beatTime>=20-delay && beatTime<=20+delay)
                    //if (Timer1 >= 9-delay1 && Timer1 <= 9+delay1)
                    //if (Timer1 >= 9.0800 -delay1 && Timer1 <= 9.0900+delay1)
                    {
                        matchedSnare[5]++;

                    }

                }



                // time runs out, stop listening for keys
                //if (total == 0)
                if (Timer1 >= 9.34 && Timer1 <= 11)
                {
                    //  if (Timer1 >= 9.3400 && Timer1 <= 9.4400) {
                    attempted = true;
                    start = false;
                    // total = 500;
                    Timer1 = 0;
                    beatTime = 560;
                }

            }



            // Checks if the attempt was correct
            if (attempted) {

                counterTimer = 3;
                counterStart = false;
                // Checks if the correct keys were pressed the correct number of times in each interval
                //@K—simpler "enhanced for loop" methodology—Nov 12, 2020 at 7.32PM
                for (int j : matchedSnare) {
                    if (j == 1) {
                        correctSnare++;
                    }
                }




                // If the sound was matched, the level is complete

                /// CHANGE TO correctSnare = countSnare in SNARE section
                if (correctSnare == countSnare) {
                    complete = true;


                }
                // Otherwise, reset the matched array for the next attempt
                else {
                    incorrectLabel = new Label(
                            "\n Snares Matched"+correctSnare+
                                    "\nTotal Snares: "+countSnare, BaseGame.labelStyle);

//                    incorrectLabel.setColor(Color.RED);
                    incorrectLabel.setColor(0,0,0,0f);
                    uiStage.addActor(incorrectLabel);
                    incorrectLabel.setPosition(100, 100);
                    incorrectLabel.addAction(Actions.delay(1));
                    incorrectLabel.addAction(Actions.after(Actions.fadeOut(4)));
                    //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
                    Arrays.fill(matchedSnare, 0);
                    correctSnare = 0;

                    attempted = false;
                }
            }

            // When the level is complete, returns goes to another level
            if (complete) {

                countKick = 6;
                countSnare = 6;
                countHat = 2;
                countBass = 1;
                correctMelody = 7;
                attempted = false;
                complete = false;
                correctKick = 0;
                correctSnare = 0;
                correctHat = 0;
                correctBass = 0;
                correctMelody =0;
                step = 2;

                //SoundByte.setActiveScreen(new completeScreen());
            }

        }

        /////STEP 2: ADD BASS AND HAT PATTERN////////////////////

        if(step ==2){

            //@K—removed "==true" as redundant—Nov 12, 2020 at 7.31PM
            if(drumPatternPlay)
            {
                drumPattern.play();
            }

            sequenceLabel.setText(" "+
                    "\nPress P \n when Time = 1, 6" +
                    "\n  "  +
                    "\nPress O when Time = 11");



            stepLabel.setText("Step 3: Add Bass and Hat");

            // Playback the sound to match

            // Try to match the playback
            if (start) {

                drumPatternPlay = false;
                drumPattern.stop();


                Timer1 += dt;

                TimeLeft.setText("Time : " + Math.round(Timer1));


                ///////////Hat—edited by @K from "SNARE"//////////////

                if (Gdx.input.isKeyJustPressed(Input.Keys.P)) {

                    ///KEY SHOULD BE PRESSED on <=x number

                    //if(beatTime>=500-delay && beatTime <=500+delay)
                    if (Timer1 <= 1)
                    {
                        matchedHat[0]++;

                    }
                    //if(beatTime>=500-delay && beatTime <=500+delay)
                    if (Timer1 >= 6-delay1 && Timer1 <= 6+delay1)

                    {
                        matchedHat[1]++;

                    }
                    //@K—then commented entirely—Nov 12, 2020 at 7.31PM
                    //else{
                    //attempted = true;
                    //start = false;
                    //Timer1 = 0;
                    //@K ——— commented below line on Nov 12.2020 @ 5PM
                    //@K ——— to have the hat not stop/be able to be clicked, even if incorrect
                    //soundHat.stop();
                    //}

                }
                if (Gdx.input.isKeyJustPressed(Input.Keys.O)) {

                    ///KEY SHOULD BE PRESSED on <=x number

                    // if(beatTime>=500-delay && beatTime <=500+delay)
                    if (Timer1>=11-delay1 && Timer1 <= 11+delay1)
                    //if (Timer1 >= 2.0400 -delay1 && Timer1 <= 2.1400 +delay1)
                    {
                        matchedBass[0]++;

                    }

                    /*
                    if (Timer1 >= 12.000 && Timer1 <= 13.000)
                    {
                        matchedBass[1]++;

                    }

                     */
                    //@K—then commented entirely—Nov 12, 2020 at 7.31PM
//                    else{
                    // attempted = true;
                    //  start = false;
                    //  Timer1 = 0;

                    //@K ——— commented below line on Nov 12.2020 @ 5PM
                    //@K ——— to have the bass not stop/be able to be clicked, even if incorrect
                    // soundBass.stop();
//                    }


                }



                // time runs out, stop listening for keys
                //if (total == 0)
                if (Timer1 >= 19.34 && Timer1 <= 20)
                {
                    //  if (Timer1 >= 9.3400 && Timer1 <= 9.4400) {
                    attempted = true;
                    start = false;
                    // total = 500;
                    Timer1 = 0;
                }

            }



            // Checks if the attempt was correct
            if (attempted) {

                counterTimer = 3;
                counterStart = false;
                // Checks if the correct keys were pressed the correct number of times in each interval
                //@K—simpler "enhanced for loop" methodology—Nov 12, 2020 at 7.32PM
                for (int j : matchedHat) {
                    if (j == 1) {
                        correctHat++;
                    }
                }

                //@K—simpler "enhanced for loop" methodology—Nov 12, 2020 at 7.32PM
                for (int bass : matchedBass) {
                    if (bass == 1) {
                        correctBass++;
                    }
                }



                // If the sound was matched, the level is complete

                if (correctHat == countHat && correctBass == countBass) {
                    complete = true;


                }
                // Otherwise, reset the matched array for the next attempt
                else {
                    incorrectLabel = new Label("Hats Matched" +correctHat +
                            "\nTotal Hats: " +countHat+
                            "\n Bass Matched " +correctBass +
                            "\nTotal Bass: " +countBass, BaseGame.labelStyle);

//                    incorrectLabel.setColor(Color.RED);
                    incorrectLabel.setColor(0,0,0,0f);
                    uiStage.addActor(incorrectLabel);
                    incorrectLabel.setPosition(100, 100);
                    incorrectLabel.addAction(Actions.delay(1));
                    incorrectLabel.addAction(Actions.after(Actions.fadeOut(4)));

                    //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
                    Arrays.fill(matchedHat, 0);
                    correctHat = 0;

                    //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
                    Arrays.fill(matchedBass, 0);
                    //@K—commented below line since value alreadys set to 0 above
//                    correctHat= 0;
                    correctBass = 0;
                    attempted = false;
                }
            }

            // When the level is complete, goes to another level
            if (complete) {

                countKick = 6;
                countSnare = 6;
                countHat = 2;
                countBass = 1;
                correctMelody = 7;
                attempted = false;
                complete = false;
                correctKick = 0;
                correctSnare = 0;
                correctHat = 0;
                correctBass = 0;
                correctMelody =0;
                step = 3;


            }

        }



        ////// STEP 3: ////////
        if(step ==3) {

            //@K—removed "==true" as redundant—Nov 12, 2020 at 7.31PM
            if(drumPatternPlay)
            {
                drumPattern.play();
            }

            //drumPattern.play();
            /////////////////////////// BEAT TIME LABEL TEXT///////////////////////////////////////////////////////////////
            // TimeLeft.setText(beatTime);



            sequenceLabel.setPosition(5,100);
            sequenceLabel.setColor(0,0,0,0f);

            // complete = false;
            sequenceLabel.setText("Seq: Q->R->E->W--->R->E->W");

            stepLabel.setPosition(10,300);
            stepLabel.setText("Step 4: Make Melody\n1.Listen melody: Press Space\n 2.Play melody using the seq \nwhen time = 1");

            //@K—removed "==true" as redundant—Nov 12, 2020 at 7.31PM
            if(melodyPatternPlay)
            {
                melodyPattern.play();
            }



            // Try to match the playback
            if (start) {

                melodyPatternPlay = false;

                melodyPattern.stop();
                Timer1 += dt;

                TimeLeft.setText("Time : " + Math.round(Timer1));



                beatTime -= dt;



                if (Gdx.input.isKeyJustPressed(Input.Keys.Q)) {
                    // First beat

                    //if ((Timer1 <= 1)|| (beatTime>500-delay && beatTime <500+delay))
                    if(beatTime>500-delay && beatTime <500+delay)
                    {
                        // Number of times key pressed in the interval
                        matchedMelody[0]++;
                    }


                  /*


                    // If off beat, stop.
                    else {
                        attempted = true;
                        start = false;
                            Timer1=0;
                    }

                   */
                }

                if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
                    // First beat

                    // Second beat
                    //if ((Timer1 >= 1 && Timer1 <= 2)||(beatTime>469-delay && beatTime <469+delay))
                    if(beatTime>469-delay && beatTime <469+delay)
                    //if(Math.round(Timer)> 10 - delay && total < 10 + delay)
                    {
                        matchedMelody[1]++;
                    }
                    // else if ((Timer1 >= 5 && Timer1 <= 6) || (beatTime>344-delay && beatTime <344+delay))
                    else if(beatTime>344-delay && beatTime <344+delay)
                    //if(Math.round(Timer)> 10 - delay && total < 10 + delay)
                    {
                        matchedMelody[4]++;
                    }
                    /*
                    // If off beat, stop.
                    else {
                        attempted = true;
                        start = false;
                        Timer1=0;
                    }

                     */
                }
                if (Gdx.input.isKeyJustPressed(Input.Keys.E)) {
                    // First beat

                    // Second beat
                    // if ((Timer1 >= 2 && Timer1 <= 3)||(beatTime>438-delay && beatTime <438+delay))
                    if(beatTime>438-delay && beatTime <438+delay)

                    {
                        matchedMelody[2]++;
                    }
                    // else if ((Timer1 >= 6 && Timer1 <= 7)||(beatTime>313-delay && beatTime <313+delay))
                    else if(beatTime>313-delay && beatTime <313+delay)
                    {
                        matchedMelody[5]++;
                    }
                    /*
                    // If off beat, stop.
                    else {
                        attempted = true;
                        start = false;
                        Timer1=0;
                    }

                     */
                }

                if (Gdx.input.isKeyJustPressed(Input.Keys.W)) {
                    // First beat

                    // Second beat
                    //if ((Timer1 >= 3 && Timer1 <= 4)||(beatTime>407-delay && beatTime <407+delay))
                    if(beatTime>407-delay && beatTime <407+delay)

                    {
                        matchedMelody[3]++;
                    }
                    //  else if ((Timer1 >= 7 && Timer1 <= 8)||(beatTime>281-delay && beatTime <281+delay))
                    else if (beatTime>281-delay && beatTime <281+delay)
                    {
                        matchedMelody[6]++;
                    }
                    /*
                    // If off beat, stop.
                    else {
                        attempted = true;
                        start = false;
                        Timer1=0;
                    }

                     */
                }


                // time runs out, stop listening for keys
                if (Timer1 >= 5 &&Timer1 <=6) {
                    attempted = true;
                    start = false;
                    Timer1=0;
                    beatTime = 560;
                }
            }

            // Checks if the attempt was correct
            if (attempted) {

                counterTimer = 3;
                counterStart = false;
                // Checks if the correct keys were pressed the correct number of times in each interval
                //@K—simpler "enhanced for loop" methodology—Nov 12, 2020 at 7.32PM
                for (int j : matchedMelody) {
                    if (j == 1) {
                        correctMelody++;
                    }
                }
                // If the sound was matched, the level is complete
                if (correctMelody == countMelody) {
                    complete = true;


                }
                // Otherwise, reset the matched array for the next attempt
                else {
                    incorrectLabel = new Label("Melody notes Matched: " + correctMelody +
                            "\nTotal melody Note: "+ countMelody, BaseGame.labelStyle);

                    incorrectLabel.setColor(Color.RED);
                    uiStage.addActor(incorrectLabel);
                    incorrectLabel.setPosition(100, 100);
                    incorrectLabel.setColor(0,0,0,0f);
                    incorrectLabel.addAction(Actions.delay(1));
                    incorrectLabel.addAction(Actions.after(Actions.fadeOut(1)));
                    //@K—simpler filling methodology—Nov 12, 2020 at 7.31PM
                    Arrays.fill(matchedMelody, 0);

                    correctMelody = 0;
                    attempted = false;
                }
            }



            // When the level is complete, goes to completeScreen
            if (complete) {

//                SoundByte.setActiveScreen(new completeScreen());
            }

        }
    }

    @Override
    public void render(float dt) {
        super.render(dt);
        //@K—MANDATORY FOR Defaults/ACTORS TO OVERLAY AND BE RENDERED AND Z-INDEXED *ON TOP OF* THE STAGE, *AFTER* THE TEXTURE/SPRITE.
        //... Invoking update simply to update the GUI; *then* overlaying/rendering the Defaults/actors with mainStage.draw() and uiStage.draw();
        //... which are overridden from the inherited class.

        //@K—main update to GUI; invocation.
        update(dt);

        mainStage.draw();
        uiStage.draw();
    }

    public boolean keyDown(int keyCode)
    {
//        if (Gdx.input.isKeyPressed(Input.Keys.ENTER))
////            setActiveScreen( new testLevel() );
//            setActiveScreen( new Level1() );
//        if(Gdx.input.isKeyPressed(Input.Keys.T))
//            setActiveScreen(new Tutorial());
        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
            Gdx.app.exit();

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
            Timer.instance().clear();
            setActiveScreen(new Launch());}
        return false;
    }
}

