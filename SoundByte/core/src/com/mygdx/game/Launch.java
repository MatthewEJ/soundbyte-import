/**
 * @author Griffins & @K—SoundByte™
 */


package com.mygdx.game;

import com.badlogic.gdx.*;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputEvent.Type;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;

//@K—for screen transitions and fades, etc. + aesthetic/audible delays
import java.util.TimerTask;

import static com.badlogic.gdx.Gdx.audio;
import static com.badlogic.gdx.utils.Timer.schedule;
import static com.mygdx.game.BaseGame.setActiveScreen;

//@K—THE 8 SOUNDS ARE DECLARED STATIC IN THE TUTORIAL SO AS TO .STOP() THEM UPON ENTERING ANOTHER LEVEL (IMPORT TUTORIAL ANYWHERE SAME SOUNDS ARE USED)
import static com.mygdx.game.Tutorial.*;

//——————————————————————//
//@K—necessary for callback functionality+utils

//——————————————————————//


public class Launch extends BaseScreen
{
    //@K—class vars for texture/sprite/batch drawing prior to actors (for proper Z-indexing)


    //@K—note: public as it's simpler to stop the sound in another .java class then redundantly pasting .stop() many times, such as for each click event;
    //... that is, so each class can access these variables (such as Tutorial.java when entering the tutorial) and stop it there immediately, as in the initialize() method.
    //@K—"Intro—"Welcome … to SoundByte™" when entering the main menu screen, as an intro. :)
    static Music musicIntro;

    //@K—Intro—ambience after entering the main menu screen/intro/"Welcome … to SoundByte™" plays.
    static Music musicIntroAmbience;

    //@K—upon clicking waveform, play audio immediately, hide other image or overlay new ani on top of already looping ani, and delay until going to level1 (until audio finishes+1s).
    static Music musicWaveformClicked;

    //@K—declare "batch" var for sprite rendering (SpriteBatch rendering, that is)
    SpriteBatch batch;

    //@K—Launch graphic
    Texture mainMenuTexture;
    Sprite mainMenuSprite;

    //@K—"EXP" graphic
    Texture expTexture;
    Sprite expSprite;

    //@K—"TUT" graphic
    Texture tutTexture;
    Sprite tutSprite;

    //@K—"LVL" graphic
    Texture lvlTexture;
    Sprite lvlSprite;

    //@K—"SET" graphic
    Texture setTexture;
    Sprite setSprite;

    //@K—"SOUNDBYTE™" graphic
    Texture soundbyteTexture;
    Sprite soundbyteSprite;

    //@K—mid-centered spectrum ani
    Sprite spectrumSprite;
    TextureAtlas spectrumAtlas;
    TextureRegion spectrumRegion;
    Animation spectrumAni;
//    int spectrumCurrentFrame; //@K—For single-frame render here, not infinitely looping animation.

    //@K—Bottom mid-centered waveform ani
    Sprite waveformSprite;
    TextureAtlas waveformAtlas;
    TextureRegion waveformRegion;
    Animation waveformAni;
//    int waveformCurrentFrame; //@K—For single-frame render here, not infinitely looping animation.

    //@K—Bottom mid-centered waveform ani—ONLY WHEN CLICKED/ACTIVATED (UPON EVENTLISTENER ACTIVITY)
    Sprite waveformClickedSprite;
    TextureAtlas waveformClickedAtlas;
    TextureRegion waveformClickedRegion;
    Animation waveformClickedAni;
    boolean waveformClickedStatus=false;
//    int waveformCurrentFrame; //@K—For single-frame render here, not infinitely looping animation.

    //@K—frame/image to render at particular point during spectrum ani
    private TextureRegion spectrumCurrentFrame;

    //@K—frame/image to render at particular point during waveform ani
    private TextureRegion waveformCurrentFrame;

    //@K—frame/image to render at particular point during waveform ani UPON CLICK
    private TextureRegion waveformClickedCurrentFrame;


    //@K—timed passed regarding frames
    private float timePassed = 0f;

    //@K—timed passed regarding frames—begin counting once clicked so as to begin at frame 0
    private float waveformTimePassed = 0f;

    //@K—positioning variables for anis/animations
//    private float aniX, aniY;

    //@K—to acquire the screen and perform relevant actions, such as not playing a sound if not on this screen
    private BaseGame SoundByteLaunch;

    //@K—static TIMER to clear all tasks—the runnables—thus preventing sounds with delay
    // ...from being audible in other levels if you jump into and out of a level quickly, as the task=cleared by then with .clear();
    static Timer kTimer;
    static TimerTask kTimerTask;


    public void create()
    {
//        //———————————————————————————————————————————————————//
//        //@K—adding smoothed/antialiased textures pertinent to texel calculations.
//        batch=new SpriteBatch();
//        mainMenu = new Texture(Gdx.files.internal("wallpapers/Level One—Reference.jpg"),true);
//        mainMenu.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
//        sprite = new Sprite(mainMenu);
//        //———————————————————antialiasing above——————————————//
    }

    public void initialize()
    {
        //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
        Timer.instance().clear();
//        //@K—need a try/catch here as the sounds generate a NullPtrException if they haven't been activated/clicked on yet, meaning they don't "exist" and are null
//        try
//        {
//            //@K—if immediately clicked into—no delay here, as audio begins immediately when main menu screen/app is opened, effectively
//            musicIntro.stop();
//
//            //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
//            musicIntroAmbience.stop();
//
//            //@K—stop all sounds as part of the actual patterns/bytes
//            soundA5.stop();
//            soundB5.stop();
//            soundC6.stop();
//            soundE6.stop();
//            soundKick.stop();
//            soundSnare.stop();
//            soundBass.stop();
//            soundHat.stop();
//        } catch (Exception e){e.printStackTrace();}


        //@K—above still generates nullptr—perform if-else clause checking


        //✳️✳️@K—CLEAR ALL TASKS IMMEDIATELY SO NO AUDIO PLAYS CROSS-LEVEL GIVEN POSSIBLE DELAY WHEN JUMPING IN AND OUT OF LEVELS
//        if(kTimer!=null){kTimer.stop();kTimer.clear();}

        //@K—if immediately clicked into this particular/current screen—no delay here, as audio begins immediately when main menu screen/app is opened, effectively
        if(musicIntro!=null) musicIntro.stop();

        //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
        if(musicIntroAmbience!=null) musicIntroAmbience.stop();

        //@K—stop all sounds as part of the actual patterns/bytes
        if(soundA5!=null) soundA5.stop();
        if(soundB5!=null) soundB5.stop();
        if(soundC6!=null) soundC6.stop();
        if(soundE6!=null) soundE6.stop();
        if(soundKick!=null) soundKick.stop();
        if(soundSnare!=null) soundSnare.stop();
        if(soundBass!=null) {soundBass.stop();}
        if(soundHat!=null) soundHat.stop();


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—stop all sounds as part of the actual patterns/bytes—MY/K's MUSIC
        if (musicKSoundByte1 != null) musicKSoundByte1.stop();
        if (musicKByte1 != null) musicKByte1.stop();
        if (musicKByte2 != null) musicKByte2.stop();
        if (musicKByte3 != null) musicKByte3.stop();
        if (musicKByte4 != null) musicKByte4.stop();
        if (musicKByte5 != null) musicKByte5.stop();
        if (musicKByte6 != null) musicKByte6.stop();
        if (musicKByte7 != null) musicKByte7.stop();
        if (musicKByte8 != null) musicKByte8.stop();
        if (musicKByte2Sound != null) musicKByte2Sound.stop();
        if (musicKByte3Sound != null) musicKByte3Sound.stop();
        if (musicKByte1Sound != null) musicKByte1Sound.stop();
        if (musicKByte4Sound != null) musicKByte4Sound.stop();
        if (musicKByte5Sound != null) musicKByte5Sound.stop();
        if (musicKByte6Sound != null) musicKByte6Sound.stop();
        if (musicKByte7Sound != null) musicKByte7Sound.stop();
        if (musicKByte8Sound != null) musicKByte8Sound.stop();



        //@K—STOP ANY ANNOUNCER DIALOGUE + BEAT AUDIO ACROSS LEVELS/EXPERIMENT/TUTORIAL——————//


        if (musicNowPlayingByte1 != null) musicNowPlayingByte1.stop();
        if (musicNowPlayingByte2 != null) musicNowPlayingByte2.stop();
        if (musicNowPlayingByte3 != null) musicNowPlayingByte3.stop();
        if (musicNowPlayingByte4 != null) musicNowPlayingByte4.stop();
        if (musicNowPlayingByte5 != null) musicNowPlayingByte5.stop();
        if (musicNowPlayingByte6 != null) musicNowPlayingByte6.stop();
        if (musicNowPlayingByte7 != null) musicNowPlayingByte7.stop();
        if (musicNowPlayingByte8 != null) musicNowPlayingByte8.stop();
        if (music3AndAHalfMinutes != null) music3AndAHalfMinutes.stop();
        if (music5Minutes != null) music5Minutes.stop();
        if (music7Minutes != null) music7Minutes.stop();
        if (musicCountdown != null) musicCountdown.stop();
        if (musicExperiment != null) musicExperiment.stop();
        if (musicSoundset != null) musicSoundset.stop();
        if (musicTutorial != null) musicTutorial.stop();
        if (musicUnsuccessful != null) musicUnsuccessful.stop();
        if (musicSuccessful != null) musicSuccessful.stop();
        if (musicVictory != null) musicVictory.stop();
        //——————//


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—my music, initialized here—ONLY IN TUTORIAL + LEVELS + EXPERIMENT (WITHOUT THE RUNNABLES/delays BELOW AS NO INTRO AUDIO, OF COURSE), AS THIS IS WHERE the music is featured
        musicKSoundByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_SOUNDBYTE_ONE.mp3"));
        musicKByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_ONE.mp3"));
        musicKByte2 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_TWO.mp3"));
        musicKByte3 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_THREE.mp3"));
        musicKByte4 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FOUR.mp3"));
        musicKByte5 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FIVE.mp3"));
        musicKByte6 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SIX.mp3"));
        musicKByte7 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SEVEN.mp3"));
        musicKByte8 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_EIGHT.mp3"));
        //@K—actual melody/one-shot sounds used in bytes above, stemmed out and appended with "Sound"
        musicKByte1Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_ONE_SOUND.mp3"));
        musicKByte2Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_TWO_SOUND.mp3"));
        musicKByte3Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_THREE_SOUND.mp3"));
        musicKByte4Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FOUR_SOUND.mp3"));
        musicKByte5Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FIVE_SOUND.mp3"));
        musicKByte6Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SIX_SOUND.mp3"));
        musicKByte7Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SEVEN_SOUND.mp3"));
        musicKByte8Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_EIGHT_SOUND.mp3"));


        //@K—IMPLEMENTATION OF DIALOGUE for this specific class (and use static declaration to .stop() these sounds like the above)
        //@K—KEEP THE STATIC IMPLEMENTATIONS **ONLY** IN TUTORIAL.JAVA (THIS IS WHERE THE STATIC VARIABLES ARE SET...
        //... SO AS TO .stop() THEM STATICALLY IN ANOTHER SCREEN—I.E., SIMPLY DON'T INCLUDE CLASS VARIABLES...
        //... FOR THESE **AND** THE ABOVE SOUNDS (ALL SOUNDS, ACTUALLY, AS PART OF THE ACTUAL BEATS/MUSIC)...
        //... IN OTHER CLASSES—ONLY KEEP THEM STATIC HERE (and commented out in other .java files' classes at the top, as class vars) OR THEY'LL BE OVERWRITTEN.
        //... I arbitrarily chose this class to keep them static—but only have them as class vars here for now.

        musicNowPlayingByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_ONE.mp3"));
        musicNowPlayingByte2 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_TWO.mp3"));
        musicNowPlayingByte3 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_THREE.mp3"));
        musicNowPlayingByte4 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_FOUR.mp3"));
        musicNowPlayingByte5 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_FIVE.mp3"));
        musicNowPlayingByte6 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_SIX.mp3"));
        musicNowPlayingByte7 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_SEVEN.mp3"));
        musicNowPlayingByte8 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_EIGHT.mp3"));
        music3AndAHalfMinutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_THREE_AND_A_HALF_MINUTES.mp3"));
        music5Minutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_FIVE_MINUTES.mp3"));
        music7Minutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_SEVEN_MINUTES.mp3"));
        musicCountdown = Gdx.audio.newMusic(Gdx.files.internal("K/K_COUNTDOWN.mp3"));
        musicExperiment = Gdx.audio.newMusic(Gdx.files.internal("K/K_EXPERIMENT.mp3"));
        musicSoundset = Gdx.audio.newMusic(Gdx.files.internal("K/K_SOUNDSET.mp3"));
        musicTutorial = Gdx.audio.newMusic(Gdx.files.internal("K/K_TUTORIAL.mp3"));
        musicUnsuccessful = Gdx.audio.newMusic(Gdx.files.internal("K/K_UNSUCCESSFUL.mp3"));
        musicSuccessful = Gdx.audio.newMusic(Gdx.files.internal("K/K_SUCCESSFUL.mp3"));
        musicVictory = Gdx.audio.newMusic(Gdx.files.internal("K/K_VICTORY.mp3"));

        //——————//


        //@K—commented out in favor of antialiased texture; actors added last in render app loop.
//        BaseActor background = new BaseActor(0,0, mainStage);
//        background.loadTexture( "wallpapers/Main Menu—Reference.jpg" );
//        background.setSize(virWidth, virHeight);


        //@K—"Intro—"Welcome … to SoundByte™" when entering the main menu screen, as an intro. :)
        musicIntro = audio.newMusic(Gdx.files.internal("K/K_INTRO_WELCOME_TO_SOUNDBYTE.mp3"));
        musicIntro.setLooping(false);
        musicIntro.play();


        //@K—"Intro—ambience after entering the main menu screen/intro/"Welcome … to SoundByte™" plays.
        musicIntroAmbience = audio.newMusic(Gdx.files.internal("K/K_INTRO_AMBIENCE.mp3"));
        musicIntroAmbience.setLooping(true);

        //@K—upon clicking waveform, play audio immediately, hide other image or overlay new ani on top of already looping ani, and delay until going to level1 (until audio finishes+1s).
        musicWaveformClicked = audio.newMusic(Gdx.files.internal("K/K_LETS_BEGIN.mp3"));
        musicWaveformClicked.setLooping(false);
//        musicIntro.play();

        //@K—scheduling the sound to come in later.
//        if(SoundByteLaunch.getScreen()=="Launch")
//        if(SoundByteLaunch!=null)
//        System.out.println(SoundByteLaunch.getScreen());


        kTimer.schedule(new com.badlogic.gdx.utils.Timer.Task() {
            @Override
            public void run() {
                //@K—arbitrary choosing of item to "hook" into to only play sound while on this screen with the runnable delay.
                //...as starting then stopping a sound doesn't always work flawlessly or immediately; prevents issue altogether since musicIntro plays immediately
                //... as having a delay introduces concerns like this
                //... ABOUT &&timePassed :: checking time in delta passed to prevent bug where jumping in and out of screens plays musicIntroAmbience since it's on a delay of 5 and avoids all checks in this instance—edge case
                //        //... at timePassed>=7.932923 is exactly when musicIntroAmbience must play
//                if(musicIntro.isPlaying()&&timePassed>=7.932923)
                //@K—hooking into something arbitrarily, such as the SoundByte™ logo which will (for now) only be on the  launch screen;
                //... since we know that the logo exists there, only play musicIntroAmbience ON THIS particular screen.
                //... primarily doing this as .getScreen() not working as intended for a BaseGame or SoundByte objects.
                //@K—but also, for the sake of the app, actually even removing this as it's preferable for ambience music (musicIntroAmbience) to play
                //... throughout the app. Although, this is a solution in case you want the inverse (i.e., ambience music [musicIntroAmbience] ONLY on the Launch screen).
//                if(musicIntro.isPlaying()&&!(soundbyteTexture==null)) //<<<<<<<<<<<<<<<<<<<<<
//                    System.out.println(timePassed);
                musicIntroAmbience.play();
            }
        }, 5);



        //@K—experimenting with fadeIns+outs
//        SequenceAction sequenceAction = new SequenceAction();
////        sequenceAction.addAction(fadeOut(0.5f));
//        sequenceAction.addAction(fadeIn(2f));
//        sequenceAction.addAction(run(new Runnable() {
////        Gdx.app.postRunnable(new Runnable() {
//            @Override
//            public void run() {
//                delay(5f);
//                musicIntroAmbience.play();
//
//            }
//
//        }));



        //———————————————————————————————————————————————————//
        //———————————————————————————————————————————————————//
        //———————————————————————————————————————————————————//

        //@K—utilizing my utils/helper class for determining size in the below section and throughout.
        //... But only works with Graphics interface as otherwise app will fail to launch with a null pointer exception/reference...
        //... since graphics haven't been rendered—this is *if* using the helper in DesktopLauncher (which is why same code ...
        //... is in DesktopLauncher but relevant to the LwjglApplicationConfiguration config instantiation of the class).
        //... Fine here, however.

        //@K—adding smoothed/antialiased textures pertinent to texel calculations.

        //@K—instantiate and initialize "batch" var for sprite rendering (SpriteBatch rendering, that is)
        batch=new SpriteBatch();

        //Launch image/sprite/texture
        //CHANGE FROM REFERENCE ONCE BUTTONS+ANIMS IMPLEMENTED
        mainMenuTexture = new Texture(Gdx.files.internal("K/K_UNIVERSAL.png"),true);
        mainMenuTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        mainMenuSprite = new Sprite(mainMenuTexture);
        mainMenuSprite.setSize(kUtils.kScreenAspectRatioWidth(), kUtils.kScreenAspectRatioHeight());

        //———————————————————————————————————————————————————//

        //"EXP" image/sprite/texture
        expTexture = new Texture(Gdx.files.internal("K/K_DARK_EXP.png"),true);
        expTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        expSprite = new Sprite(expTexture);
        expSprite.setScale(0.44f);
        expSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/10.4),(int)(kUtils.kScreenAspectRatioHeight()/43));

        //———————————————————————————————————————————————————//

        //"TUT" image/sprite/texture
        tutTexture = new Texture(Gdx.files.internal("K/K_DARK_TUT.png"),true);
        tutTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        tutSprite = new Sprite(tutTexture);
        tutSprite.setScale(0.44f);
        tutSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/9.9),(int)(kUtils.kScreenAspectRatioHeight()/43));

        //———————————————————————————————————————————————————//

        //"LVL" image/sprite/texture
        lvlTexture = new Texture(Gdx.files.internal("K/K_DARK_LVL.png"),true);
        lvlTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        lvlSprite = new Sprite(lvlTexture);
        lvlSprite.setScale(0.44f);
        lvlSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/2.105),(int)(kUtils.kScreenAspectRatioHeight()/43));

        //———————————————————————————————————————————————————//

        //"SET" image/sprite/texture
        setTexture = new Texture(Gdx.files.internal("K/K_DARK_SET.png"),true);
        setTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        setSprite = new Sprite(setTexture);
        setSprite.setScale(0.44f);
        setSprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.49),(int)(kUtils.kScreenAspectRatioHeight()/43));

        //———————————————————————————————————————————————————//

        //"SOUNDBYTE™" image/sprite/texture
        soundbyteTexture = new Texture(Gdx.files.internal("K/K_DARK_SOUNDBYTE_LOGO.png"),true);
        soundbyteTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        soundbyteSprite = new Sprite(soundbyteTexture);
        soundbyteSprite.setScale(0.44f);
        soundbyteSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/2.6),(int)(kUtils.kScreenAspectRatioHeight()/1.32));

        //———————————————————————————————————————————————————//

        //Animated waveform image/sprite/texture
        waveformAtlas = new TextureAtlas("K/K_WAVEFORM_CUSTOM.atlas");
        waveformRegion = waveformAtlas.findRegion("K_WAVEFORM_CUSTOM");
        waveformSprite=new Sprite(waveformRegion);

        //@K—acquire imgs/frames necessary for ani—multi-frame, continuously looping render/infinitely looping animation
        Array<TextureAtlas.AtlasRegion> waveformAniFrames = waveformAtlas.findRegions("K_WAVEFORM_CUSTOM");

        //@K—actual animation creation + set loop point for continuous animation
        waveformAni = new Animation(.075f, waveformAniFrames, Animation.PlayMode.LOOP);
//        waveformAni.setSize(kUtils.kScreenAspectRatioWidth()/2, kUtils.kScreenAspectRatioHeight()/2);
//        waveformAni.setPosition(kUtils.kScreenAspectRatioWidth()/4,-kUtils.kScreenAspectRatioHeight()/8);

        //———————————————————————————————————————————————————//


        //Animated waveform image/sprite/texture UPON CLICK EVENT further below
        waveformClickedAtlas = new TextureAtlas("K/K_WAVEFORM_LETS_BEGIN.atlas");
        waveformClickedRegion = waveformClickedAtlas.findRegion("K_WAVEFORM_LETS_BEGIN");
        waveformClickedSprite=new Sprite(waveformClickedRegion);

        //@K—acquire imgs/frames necessary for ani—multi-frame, continuously looping render/infinitely looping animation
        Array<TextureAtlas.AtlasRegion> waveformClickedAniFrames = waveformClickedAtlas.findRegions("K_WAVEFORM_LETS_BEGIN");

        //@K—actual animation creation + set loop point for continuous animation
        waveformClickedAni = new Animation(.075f, waveformClickedAniFrames, Animation.PlayMode.LOOP);
//        waveformAni.setSize(kUtils.kScreenAspectRatioWidth()/2, kUtils.kScreenAspectRatioHeight()/2);
//        waveformAni.setPosition(kUtils.kScreenAspectRatioWidth()/4,-kUtils.kScreenAspectRatioHeight()/8);

        //———————————————————————————————————————————————————//

        //Animated spectrum image/sprite/texture
        spectrumAtlas = new TextureAtlas("K/K_SPECTRUM_CUSTOM.atlas");
        spectrumRegion = spectrumAtlas.findRegion("K_SPECTRUM_CUSTOM");
        spectrumSprite=new Sprite(spectrumRegion);

        //@K—acquire imgs/frames necessary for ani—multi-frame, continuously looping render/infinitely looping animation
        //@K—*IN ORDER FOR REGIONS TO WORK AND DISPLAY ANI, FRAMES FIT ONTO >2048-SIZED SPRITESHEET; MAY CAUSE ISSUES ON MOBILE BUT WORKS PERFECTLY ON DESKTOP.
        Array<TextureAtlas.AtlasRegion> spectrumAniFrames = spectrumAtlas.findRegions("K_SPECTRUM_CUSTOM");

        //@K—actual animation creation + set loop point for continous animation
        spectrumAni = new Animation(.075f, spectrumAniFrames, Animation.PlayMode.LOOP);
//        waveformAni.setSize(kUtils.kScreenAspectRatioWidth()/2, kUtils.kScreenAspectRatioHeight()/2);
//        waveformAni.setPosition(kUtils.kScreenAspectRatioWidth()/4,-kUtils.kScreenAspectRatioHeight()/8);

        //———————————————————————————————————————————————————//

        //@K—extra tests for animation(s)
         //increment frame for continuous ani[mation]! :) — useful to set foreground and backgroundFPS to 60 for smoothest ani.
//        waveformSprite.setRegion(waveformAtlas.findRegion(String.format("Waveform—Custom %02d",waveformCurrentFrame))); //@K—Single frame render—need infinitely looping render

//        waveformSprite.setSize(kUtils.kScreenAspectRatioWidth()/2, kUtils.kScreenAspectRatioHeight()/2);
//        waveformSprite.setPosition(kUtils.kScreenAspectRatioWidth()/4,-kUtils.kScreenAspectRatioHeight()/8);
//      waveformSprite.setOrigin(0,0);
//       mainMenuSprite.setPosition(-sprite.getWidth()/2,-sprite.getHeight()/2);
        //———————————————————————————————————————————————————//
        //———————————————————————————————————————————————————//
        //———————————————————————————————————————————————————//

        //@K—adding a listener+empty bounding box/boundary to perform a click event, upon which transition (ideally transition, or simply switch) to level1
        //@K—but first, play K's "Let's Begin" animation visually + the audible audio to coordinated—schedule with tiemr task
        TextButton kWaveformButton = new TextButton( "Waveform", BaseGame.textButtonStyle );
        kWaveformButton.setPosition(910 * WidthOffset,73 * HeightOffset);
        kWaveformButton.setSize(115 * WidthOffset,120 * HeightOffset);
        kWaveformButton.setColor(0f,0f,0f,0f);
        uiStage.addActor(kWaveformButton);
        kWaveformButton.addListener( (Event e) -> {
            if ( !(e instanceof InputEvent) ) return false;
            if ( !((InputEvent)e).getType().equals(Type.touchDown) ) return false;

                    //@K—DISPLAY new waveform and PLAY corresponding audio for kWaveformClicked
                    waveformClickedStatus=true;
//                    musicWaveformClicked.play();

                    //@K—adding another Timer here since the audio seams off-sync/out of sync with click event upon clicking and setting true; not fully synchronous, so adding manualy delay
                    schedule(new com.badlogic.gdx.utils.Timer.Task()
                    {
                        @Override
                        public void run()
                        {
                            //@K—also need to start @ first frame here upon click
//                            delay(5f);
                            musicWaveformClicked.play();
                        }
                    }, 1);


                    //@K—scheduling the level1 transition/switch to come in later—that is, after waveform is clicked and audio fully plays out.
                    schedule(new com.badlogic.gdx.utils.Timer.Task()
                    {
                        @Override
                        public void run()
                        {
//                            waveformClickedStatus = false;

                            //@K—stop all sounds (currently active, as well)
                            //@K—adding waveformClickedStatus as part of another manual delay to make 2nd clicked waveform ani overlay disappear right after audio is complete
                            waveformClickedStatus=false; //@K—four (4) seems to be the average delay; might have to optimize code later or call update() manually to make them in parallel/synchronized/synchronous
                            musicIntro.stop();
                            musicIntroAmbience.stop();
                            setActiveScreen(new One());
                        }
                    }, 4);
                    return true; }
        );

        //———————————————————————————————————————————————————//

        //@K—set alpha to 0f for 0% opacity on buttons below—still clickable; overlaid actual clickable buttons from above.
        TextButton TutorialButton = new TextButton( "Tutorial", BaseGame.textButtonStyle );
        TutorialButton.setPosition(425 * WidthOffset,85 * HeightOffset);
        TutorialButton.setSize(350 * WidthOffset,90 * HeightOffset);
        TutorialButton.setColor(0f,0f,0f,0f);
        uiStage.addActor(TutorialButton);
        TutorialButton.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(Type.touchDown) ) return false;
                    setActiveScreen(new Tutorial()); return true; }
        );

        TextButton SandboxButton = new TextButton( "Sandbox", BaseGame.textButtonStyle );
        SandboxButton.setPosition(50 * WidthOffset,85 * HeightOffset);
        SandboxButton.setSize(350 * WidthOffset,90 * HeightOffset);
        SandboxButton.setColor(0f,0f,0f,0f);
        uiStage.addActor(SandboxButton);
        SandboxButton.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(Type.touchDown) ) return false;
                    setActiveScreen(new Experiment()); return true; }
        );

        TextButton LevelsButton = new TextButton( "Levels", BaseGame.textButtonStyle );
        LevelsButton.setPosition(1145 * WidthOffset,85 * HeightOffset);
        LevelsButton.setSize(350 * WidthOffset,90 * HeightOffset);
        LevelsButton.setColor(0f,0f,0f,0f);
        uiStage.addActor(LevelsButton);
        LevelsButton.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(Type.touchDown) ) return false;
                    setActiveScreen(new Module()); return true; }
        );

        TextButton settingsButton = new TextButton( "Settings", BaseGame.textButtonStyle );
        settingsButton.setPosition(1525 * WidthOffset,85 * HeightOffset);
        settingsButton.setSize(350 * WidthOffset,90 * HeightOffset);
        settingsButton.setColor(0f,0f,0f,0f);
        uiStage.addActor(settingsButton);
        settingsButton.addListener((Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(Type.touchDown) ) return false;
                    setActiveScreen( new Preferences() );
                   return true; }
        );

    }

    //@K—(USING LIKE "UPDATE()" METHOD)—invoke this upon clicking waveform ani rotating bottom mid-center; draws the sprite batch to display it.
    //... may instead utilize class boolean. <<
//    public void kWaveformClicked()
//    {
//
//        //@K—acquire current frame to render at this specific instant for spectrum ani texture
//        waveformClickedCurrentFrame = (TextureRegion) waveformClickedAni.getKeyFrame(timePassed);
//
//        //@K—actually render the spectrum ani! :)
//        batch.begin();
//        batch.draw(waveformClickedCurrentFrame,(int) (kUtils.kScreenAspectRatioWidth()/2.1),(int) (kUtils.kScreenAspectRatioHeight()/13.25));
//        batch.end();
//    }


    public void update(float dt)
    {
        //@K—catch-all
//        if(kTimer!=null){kTimer.stop();kTimer.clear();}

        //@K—immediately stop playing in the edge case of the 5s delay with the runnable still playing in another screen from Launch.java
        //... as we don't want the ambience music in the tutorial OR the actual levels OR the Experiment mode
        //... and the apploop is always running and update is always activating, so this will cause the sound to stop while in the level
        //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
//        if(musicIntroAmbience!=null) musicIntroAmbience.stop();

        //@K—stop all sounds as part of the actual patterns/bytes
        if(soundA5!=null) soundA5.stop();
        if(soundB5!=null) soundB5.stop();
        if(soundC6!=null) soundC6.stop();
        if(soundE6!=null) soundE6.stop();
        if(soundKick!=null) soundKick.stop();
        if(soundSnare!=null) soundSnare.stop();
        if(soundBass!=null) {soundBass.stop();}
        if(soundHat!=null) soundHat.stop();


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—stop all sounds as part of the actual patterns/bytes—MY/K's MUSIC
        if (musicKSoundByte1 != null) musicKSoundByte1.stop();
        if (musicKByte1 != null) musicKByte1.stop();
        if (musicKByte2 != null) musicKByte2.stop();
        if (musicKByte3 != null) musicKByte3.stop();
        if (musicKByte4 != null) musicKByte4.stop();
        if (musicKByte5 != null) musicKByte5.stop();
        if (musicKByte6 != null) musicKByte6.stop();
        if (musicKByte7 != null) musicKByte7.stop();
        if (musicKByte8 != null) musicKByte8.stop();
        if (musicKByte2Sound != null) musicKByte2Sound.stop();
        if (musicKByte3Sound != null) musicKByte3Sound.stop();
        if (musicKByte1Sound != null) musicKByte1Sound.stop();
        if (musicKByte4Sound != null) musicKByte4Sound.stop();
        if (musicKByte5Sound != null) musicKByte5Sound.stop();
        if (musicKByte6Sound != null) musicKByte6Sound.stop();
        if (musicKByte7Sound != null) musicKByte7Sound.stop();
        if (musicKByte8Sound != null) musicKByte8Sound.stop();



        //@K—STOP ANY ANNOUNCER DIALOGUE + BEAT AUDIO ACROSS LEVELS/EXPERIMENT/TUTORIAL——————//


        if (musicNowPlayingByte1 != null) musicNowPlayingByte1.stop();
        if (musicNowPlayingByte2 != null) musicNowPlayingByte2.stop();
        if (musicNowPlayingByte3 != null) musicNowPlayingByte3.stop();
        if (musicNowPlayingByte4 != null) musicNowPlayingByte4.stop();
        if (musicNowPlayingByte5 != null) musicNowPlayingByte5.stop();
        if (musicNowPlayingByte6 != null) musicNowPlayingByte6.stop();
        if (musicNowPlayingByte7 != null) musicNowPlayingByte7.stop();
        if (musicNowPlayingByte8 != null) musicNowPlayingByte8.stop();
        if (music3AndAHalfMinutes != null) music3AndAHalfMinutes.stop();
        if (music5Minutes != null) music5Minutes.stop();
        if (music7Minutes != null) music7Minutes.stop();
        if (musicCountdown != null) musicCountdown.stop();
        if (musicExperiment != null) musicExperiment.stop();
        if (musicSoundset != null) musicSoundset.stop();
        if (musicTutorial != null) musicTutorial.stop();
        if (musicUnsuccessful != null) musicUnsuccessful.stop();
        if (musicSuccessful != null) musicSuccessful.stop();
        if (musicVictory != null) musicVictory.stop();

        //@K—checking time in delta passed to prevent bug where jumping in and out of screens plays musicIntroAmbience since it's on a delay of 5 and avoids all checks in this instance—edge case
        //... at timePassed==9 is when musicIntroAmbience must play
//        System.out.println(timePassed);


//        callback.kResize();
//        Gdx.graphics.setWindowedMode(800,600);

        //@K—Render Launch image/sprite/texture
        batch.begin();
        mainMenuSprite.draw(batch);
        batch.end();

        //@K—Render "EXP" image/sprite/texture
        batch.begin();
        expSprite.draw(batch);
        batch.end();

        //@K—Render "TUT" image/sprite/texture
        batch.begin();
        tutSprite.draw(batch);
        batch.end();

        //@K—Render "LVL" image/sprite/texture
        batch.begin();
        lvlSprite.draw(batch);
        batch.end();

        //@K—Render "SET" image/sprite/texture
        batch.begin();
        setSprite.draw(batch);
        batch.end();

        //@K—Render "SOUNDBYTE™" image/sprite/texture
        batch.begin();
        soundbyteSprite.draw(batch);
        batch.end();

        //@K—Render animated waveform image/sprite/texture—single-frame
//        batch.begin();
//        waveformSprite.draw(batch);
//        batch.end();

        //———————————————————————————————————————————————————//

        //@K—animations initiation ...
        timePassed += Gdx.graphics.getDeltaTime();

        //@K—acquire current frame to render at this specific instant for waveform ani texture
        waveformCurrentFrame = (TextureRegion) waveformAni.getKeyFrame(timePassed);

        //@K—actually render the waveform ani! :)
        batch.begin();
        batch.draw(waveformCurrentFrame,(int) (kUtils.kScreenAspectRatioWidth()/3.7),(int) (-kUtils.kScreenAspectRatioHeight()/9.25));
        batch.end();

        //———————————————————————————————————————————————————//

        if(waveformClickedStatus)
        {
            //@K—timed passed regarding frames—begin counting once clicked so as to begin at frame 0
            waveformTimePassed += Gdx.graphics.getDeltaTime();

            //@K—RESET FRAME TO 0 SO ANI CAN PLAY at frame 1
            //@K—acquire current frame to render at this specific instant for spectrum ani texture
            waveformClickedCurrentFrame = (TextureRegion) waveformClickedAni.getKeyFrame(waveformTimePassed);

            //@K—actually render the CLICKED waveform ani! :)
            batch.begin();
            batch.draw(waveformClickedCurrentFrame,(int) (kUtils.kScreenAspectRatioWidth()/3.7),(int) (-kUtils.kScreenAspectRatioHeight()/9.25));
            batch.end();
        }

        //———————————————————————————————————————————————————//

        //@K—acquire current frame to render at this specific instant for spectrum ani texture
        spectrumCurrentFrame = (TextureRegion) spectrumAni.getKeyFrame(timePassed);

        //@K—actually render the spectrum ani! :)
        batch.begin();
        batch.draw(spectrumCurrentFrame,(int) (kUtils.kScreenAspectRatioWidth()/6.8),(int) (kUtils.kScreenAspectRatioHeight()/4.5));
        batch.end();

        //———————————————————————————————————————————————————//

//        System.out.println(-(kUtils.kScreenAspectRatioWidth())/100+"      "+kUtils.kScreenAspectRatioHeight()/8);

//        if(SoundByteLaunch!=null)
//            System.out.println(SoundByteLaunch.getScreen());
    }

    public void resize(int width, int height){
//        width = 3840;
//        height = 2160;
//        Viewport viewport.update(width, height);
    }

//    @Override
//    public void draw(Batch batch, float parentAlpha){
//        image.draw(batch, parentAlpha);

    @Override
    public void render(float dt) {
        super.render(dt);
        //@K—MANDATORY FOR BUTTONS/ACTORS TO OVERLAY AND BE RENDERED AND Z-INDEXED *ON TOP OF* THE STAGE, *AFTER* THE TEXTURE/SPRITE.
        //... Invoking update simply to update the GUI; *then* overlaying/rendering the buttons/actors with mainStage.draw() and uiStage.draw();
        //... which are overridden from the inherited class.

        //@K—main update to GUI; invocation.
        update(dt);

        mainStage.draw();
        uiStage.draw();

//        public void switchScreen(final Game game, final Screen newScreen){
//            stage.getRoot().getColor().a = 1;
//            SequenceAction sequenceAction = new SequenceAction();
//            sequenceAction.addAction(fadeOut(0.5f));
//            sequenceAction.addAction(run(new Runnable() {
//                @Override
//                public void run() {
//                    game.setScreen(newScreen);
//                }
//        @K—testing transitions
//        uiStage.getRoot().addAction(fadeIn(2f));
        //@K—using images this time; experimenting with the idea that it might *have a better quality upon render than a texture*—however, no, practically.
//        Image mainMenuImage = new Image(mainMenu);
//        mainMenuImage.setSize(kUtils.kScreenAspectRatioWidth(), kUtils.kScreenAspectRatioHeight());
//        uiStage.addActor(mainMenuImage);
        //———————————————————————//
//        @K—idea to force GUI resizing after launch: first dispose while GUI/app is launched, then re-render
//        dispose();
//        callback.kLockGUI();
//        setActiveScreen( new MenuScreen() );
//        ——————————————————
//        ——————————————————
//        @K—idea 2: tabbing to jump in/out of fullscreen
//        org.lwjgl.opengl.Display.setFullscreen;
//        Gdx.graphics.setUndecorated(false);
//        Graphics.DisplayMode currentMode = Gdx.graphics.getDisplayMode();
//        Boolean fullScreen = Gdx.graphics.isFullscreen();
//        if (Gdx.input.isKeyPressed(Keys.TAB)&&!fullScreen)
//            Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
//        else if (Gdx.input.isKeyPressed(Keys.TAB)&&fullScreen)
//            Gdx.graphics.setWindowedMode(currentMode.width, currentMode.height);

        }


    public boolean keyDown(int keyCode)
    {
        if (Gdx.input.isKeyPressed(Keys.ENTER)){
//            setActiveScreen( new testLevel() );
            setActiveScreen( new One() );//✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
            Timer.instance().clear();}
        if(Gdx.input.isKeyPressed(Keys.T)){
            setActiveScreen(new Tutorial());
            //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
            Timer.instance().clear();}
        if (Gdx.input.isKeyPressed(Keys.ESCAPE))
            Gdx.app.exit();
        return false;
    }


}