/**
 * @author Griffins & @K—SoundByte™
 */

package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Timer;

import static com.badlogic.gdx.utils.Timer.schedule;
import static com.mygdx.game.BaseGame.setActiveScreen;
import static com.mygdx.game.Launch.*;
import static com.mygdx.game.Tutorial.*;
import static com.mygdx.game.Tutorial.soundHat;

public class Preferences extends BaseScreen {

    // Difficulty variables
    private Label diff;

    public static int difficulty = 1;

    public static int getDifficulty(){
        return difficulty;
    }


    //———————————————————————————————————————————————————————//

    //@K—declare "batch" var for sprite rendering (SpriteBatch rendering, that is)
    SpriteBatch batch;

    //@K—Preferences graphic
    Texture preferencesTexture;
    Sprite preferencesSprite;

    //@K—"PREFERENCES" graphic/header
    Texture preferencesHeaderTexture;
    Sprite preferencesHeaderSprite;

    //@K—back arrow graphic/header
    Texture backArrowTexture;
    Sprite backArrowSprite;


    public void initialize() {

        //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
//        Timer.instance().clear();

//@K—need a try/catch here as the sounds generate a NullPtrException if they haven't been activated/clicked on yet, meaning they don't "exist" and are null
//        try
//        {
//            //@K—if immediately clicked into—no delay here, as audio begins immediately when main menu screen/app is opened, effectively
//            musicIntro.stop();
//
//            //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
//            musicIntroAmbience.stop();
//
//            //@K—stop all sounds as part of the actual patterns/bytes
//            soundA5.stop();
//            soundB5.stop();
//            soundC6.stop();
//            soundE6.stop();
//            soundKick.stop();
//            soundSnare.stop();
//            soundBass.stop();
//            soundHat.stop();
//        } catch (Exception e){e.printStackTrace();}


        //@K—above still generates nullptr—perform if-else clause checking

        //✳️✳️@K—CLEAR ALL TASKS IMMEDIATELY SO NO AUDIO PLAYS CROSS-LEVEL GIVEN POSSIBLE DELAY WHEN JUMPING IN AND OUT OF LEVELS
//        if(kTimer!=null){kTimer.stop();kTimer.clear();}

        //@K—if immediately clicked into this particular/current screen—no delay here, as audio begins immediately when main menu screen/app is opened, effectively
        if(musicIntro!=null) musicIntro.stop();

        //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
        if(musicIntroAmbience!=null) musicIntroAmbience.stop();

        //@K—stop all sounds as part of the actual patterns/bytes
        if(soundA5!=null) soundA5.stop();
        if(soundB5!=null) soundB5.stop();
        if(soundC6!=null) soundC6.stop();
        if(soundE6!=null) soundE6.stop();
        if(soundKick!=null) soundKick.stop();
        if(soundSnare!=null) soundSnare.stop();
        if(soundBass!=null) soundBass.stop();
        if(soundHat!=null) soundHat.stop();


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—stop all sounds as part of the actual patterns/bytes—MY/K's MUSIC
        if (musicKSoundByte1 != null) musicKSoundByte1.stop();
        if (musicKByte1 != null) musicKByte1.stop();
        if (musicKByte2 != null) musicKByte2.stop();
        if (musicKByte3 != null) musicKByte3.stop();
        if (musicKByte4 != null) musicKByte4.stop();
        if (musicKByte5 != null) musicKByte5.stop();
        if (musicKByte6 != null) musicKByte6.stop();
        if (musicKByte7 != null) musicKByte7.stop();
        if (musicKByte8 != null) musicKByte8.stop();
        if (musicKByte2Sound != null) musicKByte2Sound.stop();
        if (musicKByte3Sound != null) musicKByte3Sound.stop();
        if (musicKByte1Sound != null) musicKByte1Sound.stop();
        if (musicKByte4Sound != null) musicKByte4Sound.stop();
        if (musicKByte5Sound != null) musicKByte5Sound.stop();
        if (musicKByte6Sound != null) musicKByte6Sound.stop();
        if (musicKByte7Sound != null) musicKByte7Sound.stop();
        if (musicKByte8Sound != null) musicKByte8Sound.stop();



        //@K—STOP ANY ANNOUNCER DIALOGUE + BEAT AUDIO ACROSS LEVELS/EXPERIMENT/TUTORIAL——————//


        if (musicNowPlayingByte1 != null) musicNowPlayingByte1.stop();
        if (musicNowPlayingByte2 != null) musicNowPlayingByte2.stop();
        if (musicNowPlayingByte3 != null) musicNowPlayingByte3.stop();
        if (musicNowPlayingByte4 != null) musicNowPlayingByte4.stop();
        if (musicNowPlayingByte5 != null) musicNowPlayingByte5.stop();
        if (musicNowPlayingByte6 != null) musicNowPlayingByte6.stop();
        if (musicNowPlayingByte7 != null) musicNowPlayingByte7.stop();
        if (musicNowPlayingByte8 != null) musicNowPlayingByte8.stop();
        if (music3AndAHalfMinutes != null) music3AndAHalfMinutes.stop();
        if (music5Minutes != null) music5Minutes.stop();
        if (music7Minutes != null) music7Minutes.stop();
        if (musicCountdown != null) musicCountdown.stop();
        if (musicExperiment != null) musicExperiment.stop();
        if (musicSoundset != null) musicSoundset.stop();
        if (musicTutorial != null) musicTutorial.stop();
        if (musicUnsuccessful != null) musicUnsuccessful.stop();
        if (musicSuccessful != null) musicSuccessful.stop();
        if (musicVictory != null) musicVictory.stop();
        //——————//


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—my music, initialized here—ONLY IN TUTORIAL + LEVELS + EXPERIMENT (WITHOUT THE RUNNABLES/delays BELOW AS NO INTRO AUDIO, OF COURSE), AS THIS IS WHERE the music is featured
        musicKSoundByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_SOUNDBYTE_ONE.mp3"));
        musicKByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_ONE.mp3"));
        musicKByte2 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_TWO.mp3"));
        musicKByte3 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_THREE.mp3"));
        musicKByte4 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FOUR.mp3"));
        musicKByte5 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FIVE.mp3"));
        musicKByte6 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SIX.mp3"));
        musicKByte7 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SEVEN.mp3"));
        musicKByte8 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_EIGHT.mp3"));
        //@K—actual melody/one-shot sounds used in bytes above, stemmed out and appended with "Sound"
        musicKByte1Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_ONE_SOUND.mp3"));
        musicKByte2Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_TWO_SOUND.mp3"));
        musicKByte3Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_THREE_SOUND.mp3"));
        musicKByte4Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FOUR_SOUND.mp3"));
        musicKByte5Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FIVE_SOUND.mp3"));
        musicKByte6Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SIX_SOUND.mp3"));
        musicKByte7Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SEVEN_SOUND.mp3"));
        musicKByte8Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_EIGHT_SOUND.mp3"));


        //@K—IMPLEMENTATION OF DIALOGUE for this specific class (and use static declaration to .stop() these sounds like the above)
        //@K—KEEP THE STATIC IMPLEMENTATIONS **ONLY** IN TUTORIAL.JAVA (THIS IS WHERE THE STATIC VARIABLES ARE SET...
        //... SO AS TO .stop() THEM STATICALLY IN ANOTHER SCREEN—I.E., SIMPLY DON'T INCLUDE CLASS VARIABLES...
        //... FOR THESE **AND** THE ABOVE SOUNDS (ALL SOUNDS, ACTUALLY, AS PART OF THE ACTUAL BEATS/MUSIC)...
        //... IN OTHER CLASSES—ONLY KEEP THEM STATIC HERE (and commented out in other .java files' classes at the top, as class vars) OR THEY'LL BE OVERWRITTEN.
        //... I arbitrarily chose this class to keep them static—but only have them as class vars here for now.

        musicNowPlayingByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_ONE.mp3"));
        musicNowPlayingByte2 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_TWO.mp3"));
        musicNowPlayingByte3 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_THREE.mp3"));
        musicNowPlayingByte4 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_FOUR.mp3"));
        musicNowPlayingByte5 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_FIVE.mp3"));
        musicNowPlayingByte6 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_SIX.mp3"));
        musicNowPlayingByte7 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_SEVEN.mp3"));
        musicNowPlayingByte8 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_EIGHT.mp3"));
        music3AndAHalfMinutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_THREE_AND_A_HALF_MINUTES.mp3"));
        music5Minutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_FIVE_MINUTES.mp3"));
        music7Minutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_SEVEN_MINUTES.mp3"));
        musicCountdown = Gdx.audio.newMusic(Gdx.files.internal("K/K_COUNTDOWN.mp3"));
        musicExperiment = Gdx.audio.newMusic(Gdx.files.internal("K/K_EXPERIMENT.mp3"));
        musicSoundset = Gdx.audio.newMusic(Gdx.files.internal("K/K_SOUNDSET.mp3"));
        musicTutorial = Gdx.audio.newMusic(Gdx.files.internal("K/K_TUTORIAL.mp3"));
        musicUnsuccessful = Gdx.audio.newMusic(Gdx.files.internal("K/K_UNSUCCESSFUL.mp3"));
        musicSuccessful = Gdx.audio.newMusic(Gdx.files.internal("K/K_SUCCESSFUL.mp3"));
        musicVictory = Gdx.audio.newMusic(Gdx.files.internal("K/K_VICTORY.mp3"));

        //——————//


        BaseActor background = new BaseActor(0,0, mainStage);
//        background.loadTexture( "wallpapers/Main Menu—Reference.jpg" );
        background.setSize(virWidth,virHeight);




        //@K—instantiate and initialize "batch" var for sprite rendering (SpriteBatch rendering, that is)
        batch=new SpriteBatch();

        //Preferences image/sprite/texture
        //CHANGE FROM REFERENCE ONCE BUTTONS+ANIMS IMPLEMENTED
        preferencesTexture = new Texture(Gdx.files.internal("K/K_UNIVERSAL.png"),true);
        preferencesTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        preferencesSprite = new Sprite(preferencesTexture);
        preferencesSprite.setSize(kUtils.kScreenAspectRatioWidth(), kUtils.kScreenAspectRatioHeight());
//        tutorialSprite.setScale(0.44f);
//        tutorialSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/2.6),(int)(kUtils.kScreenAspectRatioHeight()/1.32));

        //"PREFERENCES" image/sprite/texture
        preferencesHeaderTexture = new Texture(Gdx.files.internal("K/K_DARK_PREFERENCES.png"),true);
        preferencesHeaderTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        preferencesHeaderSprite = new Sprite(preferencesHeaderTexture);
        preferencesHeaderSprite.setScale(0.44f);
        preferencesHeaderSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/2.225),(int)(kUtils.kScreenAspectRatioHeight()/1.319));

        //Back arrow image/sprite/texture
        backArrowTexture = new Texture(Gdx.files.internal("K/K_DARK_ARROW_PRV.png"),true);
        backArrowTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        backArrowSprite = new Sprite(backArrowTexture);
        backArrowSprite.setScale(.175f);
        backArrowSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/6.15),(int)(kUtils.kScreenAspectRatioHeight()/1.14));





        //@K—go to Launch✳️⬅️
        //@K—go to Launch✳️⬅️
        //@K—go to Launch✳️⬅️
//        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton menuButton = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
        menuButton.setColor(0,0,0,0f);
//        menuButton.setPosition(600 * WidthOffset,450 * HeightOffset);
        menuButton.setPosition((int)(kUtils.kScreenAspectRatioWidth()/105.15),(int)(kUtils.kScreenAspectRatioHeight()/1.0625));
        menuButton.setSize((int)(kUtils.kScreenAspectRatioWidth()/12.15),(int)(kUtils.kScreenAspectRatioWidth()/70.15));
        uiStage.addActor(menuButton);
        menuButton.addListener( (Event e) -> {
            if ( !(e instanceof InputEvent) ) return false;
            if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

            //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
            Timer.instance().clear();

            SoundByte.setActiveScreen(new Launch());return true; }
        );

//        TextButton helpButton = new TextButton( "Help", BaseGame.textButtonStyle );
//        helpButton.setPosition(50 * WidthOffset,600 * HeightOffset);
////        helpButton.setColor(Color.BLACK);
//        helpButton.setColor(0f,0f,0f,0f);
//        uiStage.addActor(helpButton);
//        helpButton.addListener( (Event e) -> {
//                    if ( !(e instanceof InputEvent) ) return false;
//                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;
//                    SoundByte.setActiveScreen( new Help() );return true; }
//        );


        /////// Difficulty options
//        TextButton plusButton = new TextButton("+", BaseGame.textButtonStyle);
//        plusButton.setColor(Color.WHITE);
//        plusButton.setSize(50 * WidthOffset, 50 * HeightOffset);
////        plusButton.setPosition(50 * WidthOffset, 400 * HeightOffset);
//        uiStage.addActor(plusButton);
//        plusButton.addListener( (Event e) -> {
//                    if ( !(e instanceof InputEvent) ) return false;
//                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;
//                    if(difficulty < 3) { difficulty ++; } return true; }
//        );

//        TextButton minusButton = new TextButton("-", BaseGame.textButtonStyle);
//        minusButton.setColor(Color.WHITE);
//        minusButton.setSize(50 * WidthOffset, 50 * HeightOffset);
////        minusButton.setPosition(100 * WidthOffset, 400 * HeightOffset);
//        uiStage.addActor(minusButton);
//        minusButton.addListener( (Event e) -> {
//                    if ( !(e instanceof InputEvent) ) return false;
//                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;
//                    if(difficulty > 1) { difficulty --; } return true; }
//        );

////        diff = new Label("Difficulty: " + difficulty, BaseGame.labelStyle);
//        diff.setPosition(40 * WidthOffset,500 * HeightOffset);
//        diff.setColor(Color.WHITE);
//        uiStage.addActor(diff);
    }


    @Override
    public void render(float dt) {
        super.render(dt);

        //@K—catchall
//        if(kTimer!=null){kTimer.stop();kTimer.clear();}

        //@K—MANDATORY FOR Defaults/ACTORS TO OVERLAY AND BE RENDERED AND Z-INDEXED *ON TOP OF* THE STAGE, *AFTER* THE TEXTURE/SPRITE.
        //... Invoking update simply to update the GUI; *then* overlaying/rendering the Defaults/actors with mainStage.draw() and uiStage.draw();
        //... which are overridden from the inherited class.

        //@K—main update to GUI; invocation.
        update(dt);

        mainStage.draw();
        uiStage.draw();
    }


    public void update(float dt) {
        //@K—immediately stop playing in the edge case of the 5s delay with the runnable still playing in another screen from Launch.java
        //... as we don't want the ambience music in the tutorial OR the actual levels OR the Experiment mode
        //... and the apploop is always running and update is always activating, so this will cause the sound to stop while in the level
        //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
//        if(musicIntroAmbience!=null) musicIntroAmbience.stop();

        //@K—stop all sounds as part of the actual patterns/bytes
        if(soundA5!=null) soundA5.stop();
        if(soundB5!=null) soundB5.stop();
        if(soundC6!=null) soundC6.stop();
        if(soundE6!=null) soundE6.stop();
        if(soundKick!=null) soundKick.stop();
        if(soundSnare!=null) soundSnare.stop();
        if(soundBass!=null) {soundBass.stop();}
        if(soundHat!=null) soundHat.stop();


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—stop all sounds as part of the actual patterns/bytes—MY/K's MUSIC
//        if (musicKSoundByte1 != null) musicKSoundByte1.stop();
        if (musicKByte1 != null) musicKByte1.stop();
        if (musicKByte2 != null) musicKByte2.stop();
        if (musicKByte3 != null) musicKByte3.stop();
        if (musicKByte4 != null) musicKByte4.stop();
        if (musicKByte5 != null) musicKByte5.stop();
        if (musicKByte6 != null) musicKByte6.stop();
        if (musicKByte7 != null) musicKByte7.stop();
        if (musicKByte8 != null) musicKByte8.stop();
//        if (musicKByte2Sound != null) musicKByte2Sound.stop();
//        if (musicKByte3Sound != null) musicKByte3Sound.stop();
//        if (musicKByte1Sound != null) musicKByte1Sound.stop();
//        if (musicKByte4Sound != null) musicKByte4Sound.stop();
//        if (musicKByte5Sound != null) musicKByte5Sound.stop();
//        if (musicKByte6Sound != null) musicKByte6Sound.stop();
//        if (musicKByte7Sound != null) musicKByte7Sound.stop();
//        if (musicKByte8Sound != null) musicKByte8Sound.stop();



        //@K—STOP ANY ANNOUNCER DIALOGUE + BEAT AUDIO ACROSS LEVELS/EXPERIMENT/TUTORIAL——————//


        if (musicNowPlayingByte1 != null) musicNowPlayingByte1.stop();
        if (musicNowPlayingByte2 != null) musicNowPlayingByte2.stop();
        if (musicNowPlayingByte3 != null) musicNowPlayingByte3.stop();
        if (musicNowPlayingByte4 != null) musicNowPlayingByte4.stop();
        if (musicNowPlayingByte5 != null) musicNowPlayingByte5.stop();
        if (musicNowPlayingByte6 != null) musicNowPlayingByte6.stop();
        if (musicNowPlayingByte7 != null) musicNowPlayingByte7.stop();
        if (musicNowPlayingByte8 != null) musicNowPlayingByte8.stop();
        if (music3AndAHalfMinutes != null) music3AndAHalfMinutes.stop();
        if (music5Minutes != null) music5Minutes.stop();
        if (music7Minutes != null) music7Minutes.stop();
        if (musicCountdown != null) musicCountdown.stop();
//        if (musicExperiment != null) musicExperiment.stop();
//        if (musicSoundset != null) musicSoundset.stop();
        if (musicTutorial != null) musicTutorial.stop();
        if (musicUnsuccessful != null) musicUnsuccessful.stop();
        if (musicSuccessful != null) musicSuccessful.stop();
        if (musicVictory != null) musicVictory.stop();


        //@K—Render Preferences image/sprite/texture
        batch.begin();
        preferencesSprite.draw(batch);
        batch.end();

        //@K—Render "PREFERENCES" image/sprite/texture
        batch.begin();
        preferencesHeaderSprite.draw(batch);
        batch.end();

        //@K—Render back arrow image/sprite/texture
        batch.begin();
        backArrowSprite.draw(batch);
        batch.end();

//        diff.setText("Difficulty: " + difficulty);

    }

    public boolean keyDown(int keyCode)
    {
//        if (Gdx.input.isKeyPressed(Input.Keys.ENTER))
////            setActiveScreen( new testLevel() );
//            setActiveScreen( new Level1() );
//        if(Gdx.input.isKeyPressed(Input.Keys.T))
//            setActiveScreen(new Tutorial());
        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
            Gdx.app.exit();

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
            Timer.instance().clear();
            setActiveScreen(new Launch());}
        return false;
    }


}
