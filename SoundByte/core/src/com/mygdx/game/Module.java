/**
 * @author Griffins & @K—SoundByte™
 */

package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Timer;

import static com.badlogic.gdx.utils.Timer.schedule;
import static com.mygdx.game.BaseGame.setActiveScreen;
//@K—THE 8 SOUNDS ARE DECLARED STATIC IN THE TUTORIAL SO AS TO .STOP() THEM UPON ENTERING ANOTHER LEVEL (IMPORT TUTORIAL ANYWHERE SAME SOUNDS ARE USED)
import static com.mygdx.game.Launch.*;
import static com.mygdx.game.Tutorial.*;
import static com.mygdx.game.Tutorial.soundHat;

public class Module extends BaseScreen {


    //@K—declare "batch" var for sprite rendering (SpriteBatch rendering, that is)
    SpriteBatch batch;


    //@K—Level graphic/header
    Texture levelTexture;
    Sprite levelSprite;

    //@K—Level graphic/header
    Texture levelHeaderTexture;
    Sprite levelHeaderSprite;

    //@K—Level 1 graphic
    Texture lvl1Texture;
    Sprite lvl1Sprite;

    //@K—Level 2 graphic
    Texture lvl2Texture;
    Sprite lvl2Sprite;

    //@K—Level 3 graphic
    Texture lvl3Texture;
    Sprite lvl3Sprite;

    //@K—back arrow graphic/header
    Texture backArrowTexture;
    Sprite backArrowSprite;


    public void initialize() {
        //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
//        Timer.instance().clear();

        //@K—need a try/catch here as the sounds generate a NullPtrException if they haven't been activated/clicked on yet, meaning they don't "exist" and are null
//        try
//        {
//            //@K—if immediately clicked into—no delay here, as audio begins immediately when main menu screen/app is opened, effectively
//            musicIntro.stop();
//
//            //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
//            musicIntroAmbience.stop();
//
//            //@K—stop all sounds as part of the actual patterns/bytes
//            soundA5.stop();
//            soundB5.stop();
//            soundC6.stop();
//            soundE6.stop();
//            soundKick.stop();
//            soundSnare.stop();
//            soundBass.stop();
//            soundHat.stop();
//        } catch (Exception e){e.printStackTrace();}


        //@K—above still generates nullptr—perform if-else clause checking

        //✳️✳️@K—CLEAR ALL TASKS IMMEDIATELY SO NO AUDIO PLAYS CROSS-LEVEL GIVEN POSSIBLE DELAY WHEN JUMPING IN AND OUT OF LEVELS
//        if(kTimer!=null){kTimer.stop();kTimer.clear();}

        //@K—if immediately clicked into this particular/current screen—no delay here, as audio begins immediately when main menu screen/app is opened, effectively
        if(musicIntro!=null) musicIntro.stop();

        //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
        if(musicIntroAmbience!=null) musicIntroAmbience.stop();

        //@K—stop all sounds as part of the actual patterns/bytes
        if(soundA5!=null) soundA5.stop();
        if(soundB5!=null) soundB5.stop();
        if(soundC6!=null) soundC6.stop();
        if(soundE6!=null) soundE6.stop();
        if(soundKick!=null) soundKick.stop();
        if(soundSnare!=null) soundSnare.stop();
        if(soundBass!=null) soundBass.stop();
        if(soundHat!=null) soundHat.stop();

        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—stop all sounds as part of the actual patterns/bytes—MY/K's MUSIC
        if (musicKSoundByte1 != null) musicKSoundByte1.stop();
        if (musicKByte1 != null) musicKByte1.stop();
        if (musicKByte2 != null) musicKByte2.stop();
        if (musicKByte3 != null) musicKByte3.stop();
        if (musicKByte4 != null) musicKByte4.stop();
        if (musicKByte5 != null) musicKByte5.stop();
        if (musicKByte6 != null) musicKByte6.stop();
        if (musicKByte7 != null) musicKByte7.stop();
        if (musicKByte8 != null) musicKByte8.stop();
        if (musicKByte2Sound != null) musicKByte2Sound.stop();
        if (musicKByte3Sound != null) musicKByte3Sound.stop();
        if (musicKByte1Sound != null) musicKByte1Sound.stop();
        if (musicKByte4Sound != null) musicKByte4Sound.stop();
        if (musicKByte5Sound != null) musicKByte5Sound.stop();
        if (musicKByte6Sound != null) musicKByte6Sound.stop();
        if (musicKByte7Sound != null) musicKByte7Sound.stop();
        if (musicKByte8Sound != null) musicKByte8Sound.stop();



        //@K—STOP ANY ANNOUNCER DIALOGUE + BEAT AUDIO ACROSS LEVELS/EXPERIMENT/TUTORIAL——————//


        if (musicNowPlayingByte1 != null) musicNowPlayingByte1.stop();
        if (musicNowPlayingByte2 != null) musicNowPlayingByte2.stop();
        if (musicNowPlayingByte3 != null) musicNowPlayingByte3.stop();
        if (musicNowPlayingByte4 != null) musicNowPlayingByte4.stop();
        if (musicNowPlayingByte5 != null) musicNowPlayingByte5.stop();
        if (musicNowPlayingByte6 != null) musicNowPlayingByte6.stop();
        if (musicNowPlayingByte7 != null) musicNowPlayingByte7.stop();
        if (musicNowPlayingByte8 != null) musicNowPlayingByte8.stop();
        if (music3AndAHalfMinutes != null) music3AndAHalfMinutes.stop();
        if (music5Minutes != null) music5Minutes.stop();
        if (music7Minutes != null) music7Minutes.stop();
        if (musicCountdown != null) musicCountdown.stop();
        if (musicExperiment != null) musicExperiment.stop();
        if (musicSoundset != null) musicSoundset.stop();
        if (musicTutorial != null) musicTutorial.stop();
        if (musicUnsuccessful != null) musicUnsuccessful.stop();
        if (musicSuccessful != null) musicSuccessful.stop();
        if (musicVictory != null) musicVictory.stop();
        //——————//


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—my music, initialized here—ONLY IN TUTORIAL + LEVELS + EXPERIMENT (WITHOUT THE RUNNABLES/delays BELOW AS NO INTRO AUDIO, OF COURSE), AS THIS IS WHERE the music is featured
        musicKSoundByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_SOUNDBYTE_ONE.mp3"));
        musicKByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_ONE.mp3"));
        musicKByte2 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_TWO.mp3"));
        musicKByte3 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_THREE.mp3"));
        musicKByte4 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FOUR.mp3"));
        musicKByte5 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FIVE.mp3"));
        musicKByte6 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SIX.mp3"));
        musicKByte7 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SEVEN.mp3"));
        musicKByte8 = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_EIGHT.mp3"));
        //@K—actual melody/one-shot sounds used in bytes above, stemmed out and appended with "Sound"
        musicKByte1Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_ONE_SOUND.mp3"));
        musicKByte2Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_TWO_SOUND.mp3"));
        musicKByte3Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_THREE_SOUND.mp3"));
        musicKByte4Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FOUR_SOUND.mp3"));
        musicKByte5Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_FIVE_SOUND.mp3"));
        musicKByte6Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SIX_SOUND.mp3"));
        musicKByte7Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_SEVEN_SOUND.mp3"));
        musicKByte8Sound = Gdx.audio.newMusic(Gdx.files.internal("K/K_BYTE_EIGHT_SOUND.mp3"));


        //@K—IMPLEMENTATION OF DIALOGUE for this specific class (and use static declaration to .stop() these sounds like the above)
        //@K—KEEP THE STATIC IMPLEMENTATIONS **ONLY** IN TUTORIAL.JAVA (THIS IS WHERE THE STATIC VARIABLES ARE SET...
        //... SO AS TO .stop() THEM STATICALLY IN ANOTHER SCREEN—I.E., SIMPLY DON'T INCLUDE CLASS VARIABLES...
        //... FOR THESE **AND** THE ABOVE SOUNDS (ALL SOUNDS, ACTUALLY, AS PART OF THE ACTUAL BEATS/MUSIC)...
        //... IN OTHER CLASSES—ONLY KEEP THEM STATIC HERE (and commented out in other .java files' classes at the top, as class vars) OR THEY'LL BE OVERWRITTEN.
        //... I arbitrarily chose this class to keep them static—but only have them as class vars here for now.

        musicNowPlayingByte1 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_ONE.mp3"));
        musicNowPlayingByte2 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_TWO.mp3"));
        musicNowPlayingByte3 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_THREE.mp3"));
        musicNowPlayingByte4 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_FOUR.mp3"));
        musicNowPlayingByte5 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_FIVE.mp3"));
        musicNowPlayingByte6 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_SIX.mp3"));
        musicNowPlayingByte7 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_SEVEN.mp3"));
        musicNowPlayingByte8 = Gdx.audio.newMusic(Gdx.files.internal("K/K_NOW_PLAYING_BYTE_EIGHT.mp3"));
        music3AndAHalfMinutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_THREE_AND_A_HALF_MINUTES.mp3"));
        music5Minutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_FIVE_MINUTES.mp3"));
        music7Minutes = Gdx.audio.newMusic(Gdx.files.internal("K/K_SEVEN_MINUTES.mp3"));
        musicCountdown = Gdx.audio.newMusic(Gdx.files.internal("K/K_COUNTDOWN.mp3"));
        musicExperiment = Gdx.audio.newMusic(Gdx.files.internal("K/K_EXPERIMENT.mp3"));
        musicSoundset = Gdx.audio.newMusic(Gdx.files.internal("K/K_SOUNDSET.mp3"));
        musicTutorial = Gdx.audio.newMusic(Gdx.files.internal("K/K_TUTORIAL.mp3"));
        musicUnsuccessful = Gdx.audio.newMusic(Gdx.files.internal("K/K_UNSUCCESSFUL.mp3"));
        musicSuccessful = Gdx.audio.newMusic(Gdx.files.internal("K/K_SUCCESSFUL.mp3"));
        musicVictory = Gdx.audio.newMusic(Gdx.files.internal("K/K_VICTORY.mp3"));

        //——————//


        //@K—instantiate and initialize "batch" var for sprite rendering (SpriteBatch rendering, that is)
        batch=new SpriteBatch();

        //Level image/sprite/texture
        levelTexture = new Texture(Gdx.files.internal("K/K_UNIVERSAL.png"),true);
        levelTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        levelSprite = new Sprite(levelTexture);
        levelSprite.setSize(kUtils.kScreenAspectRatioWidth(), kUtils.kScreenAspectRatioHeight());
//        levelSprite.setScale(0.44f);
//        levelSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/2.6),(int)(kUtils.kScreenAspectRatioHeight()/1.32));

        levelHeaderTexture = new Texture(Gdx.files.internal("K/K_DARK_MODULE.png"),true);
        levelHeaderTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        levelHeaderSprite = new Sprite(levelHeaderTexture);
        levelHeaderSprite.setScale(.44f);
        levelHeaderSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/23.7),(int)(kUtils.kScreenAspectRatioHeight()/1.32));


        lvl1Texture = new Texture(Gdx.files.internal("K/K_DARK_LVL_ONE.png"),true);
        lvl1Texture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        lvl1Sprite = new Sprite(lvl1Texture);
        lvl1Sprite.setScale(.44f);
        lvl1Sprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/6.35),(int)(-kUtils.kScreenAspectRatioHeight()/19.75));

        lvl2Texture = new Texture(Gdx.files.internal("K/K_DARK_LVL_TWO.png"),true);
        lvl2Texture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        lvl2Sprite = new Sprite(lvl2Texture);
        lvl2Sprite.setScale(.44f);
        lvl2Sprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/5.2475),(int)(-kUtils.kScreenAspectRatioHeight()/19.75));

        lvl3Texture = new Texture(Gdx.files.internal("K/K_DARK_LVL_THREE.png"),true);
        lvl3Texture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        lvl3Sprite = new Sprite(lvl3Texture);
        lvl3Sprite.setScale(.44f);
        lvl3Sprite.setPosition((int)(kUtils.kScreenAspectRatioWidth()/1.86),(int)(-kUtils.kScreenAspectRatioHeight()/19.75));

        backArrowTexture = new Texture(Gdx.files.internal("K/K_DARK_ARROW_PRV.png"),true);
        backArrowTexture.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.MipMap);
        backArrowSprite = new Sprite(backArrowTexture);
        backArrowSprite.setScale(.175f);
        backArrowSprite.setPosition((int)(-kUtils.kScreenAspectRatioWidth()/6.15),(int)(kUtils.kScreenAspectRatioHeight()/1.14));





        BaseActor background = new BaseActor(0,0, mainStage);
//        background.loadTexture( "K/Universal.png" );
        background.setSize(virWidth,virHeight);

        TextButton OneButton = new TextButton( "1", BaseGame.textButtonStyle );
        OneButton.setPosition(35 * WidthOffset,280 * HeightOffset);
        OneButton.setSize(520 * WidthOffset,520 * HeightOffset);
//        OneButton.setColor(1f, 1f, 1f, 1f);
        OneButton.setColor(0f,0f,0f,0f);
        uiStage.addActor(OneButton);
        OneButton.addListener((Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;
                    SoundByte.setActiveScreen(new One()); return true; }
        );

        TextButton TwoButton = new TextButton( "2", BaseGame.textButtonStyle );
        TwoButton.setPosition(700 * WidthOffset,280 * HeightOffset);
        TwoButton.setSize(520 * WidthOffset,520 * HeightOffset);
//        TwoButton.setColor(1f, 1f, 1f, 1f);
        TwoButton.setColor(0f,0f,0f,0f);
        uiStage.addActor(TwoButton);
        TwoButton.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;
                    SoundByte.setActiveScreen(new Two());return true; }
        );

        TextButton ThreeButton = new TextButton( "3", BaseGame.textButtonStyle );
        ThreeButton.setPosition(1365 * WidthOffset,280 * HeightOffset);
        ThreeButton.setSize(520 * WidthOffset,520 * HeightOffset);
//        ThreeButton.setColor(1f, 1f, 1f, 1f);
        ThreeButton.setColor(0f,0f,0f,0f);
        uiStage.addActor(ThreeButton);
        ThreeButton.addListener( (Event e) -> {
                    if ( !(e instanceof InputEvent) ) return false;
                    if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;
                    SoundByte.setActiveScreen(new Three());return true; }
        );



        //@K—go to Launch✳️⬅️
        //@K—go to Launch✳️⬅️
        //@K—go to Launch✳️⬅️
        //        TextButton menuButton = new TextButton( "Back", BaseGame.textButtonStyle);
        TextButton menuButton = new TextButton( "", BaseGame.textButtonStyle);
//        menuButton.setColor(Color.WHITE);
        menuButton.setColor(0,0,0,0f);
//        menuButton.setPosition(600 * WidthOffset,450 * HeightOffset);
        menuButton.setPosition((int)(kUtils.kScreenAspectRatioWidth()/105.15),(int)(kUtils.kScreenAspectRatioHeight()/1.0625));
        menuButton.setSize((int)(kUtils.kScreenAspectRatioWidth()/12.15),(int)(kUtils.kScreenAspectRatioWidth()/70.15));
        uiStage.addActor(menuButton);
        menuButton.addListener( (Event e) -> {
            if ( !(e instanceof InputEvent) ) return false;
            if ( !((InputEvent)e).getType().equals(InputEvent.Type.touchDown) ) return false;

            //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
            Timer.instance().clear();

            SoundByte.setActiveScreen(new Launch());return true; }
        );
    }

    public void update(float dt) {

//        if(kTimer!=null){kTimer.stop();kTimer.clear();}

        //@K—immediately stop playing in the edge case of the 5s delay with the runnable still playing in another screen from Launch.java
        //... as we don't want the ambience music in the tutorial OR the actual levels OR the Experiment mode
        //... and the apploop is always running and update is always activating, so this will cause the sound to stop while in the level
        //@K—if immediately clicked into—otherwise, delay occurs given the runnable in the main menu screen class at 5s
//        if(musicIntroAmbience!=null) musicIntroAmbience.stop();

        //@K—stop all sounds as part of the actual patterns/bytes
        if(soundA5!=null) soundA5.stop();
        if(soundB5!=null) soundB5.stop();
        if(soundC6!=null) soundC6.stop();
        if(soundE6!=null) soundE6.stop();
        if(soundKick!=null) soundKick.stop();
        if(soundSnare!=null) soundSnare.stop();
        if(soundBass!=null) {soundBass.stop();}
        if(soundHat!=null) soundHat.stop();


        //✳️✳️✳️
        //✳️✳️✳️
        //✳️✳️✳️
        //@K—stop all sounds as part of the actual patterns/bytes—MY/K's MUSIC
//        if (musicKSoundByte1 != null) musicKSoundByte1.stop();
        if (musicKByte1 != null) musicKByte1.stop();
        if (musicKByte2 != null) musicKByte2.stop();
        if (musicKByte3 != null) musicKByte3.stop();
        if (musicKByte4 != null) musicKByte4.stop();
        if (musicKByte5 != null) musicKByte5.stop();
        if (musicKByte6 != null) musicKByte6.stop();
        if (musicKByte7 != null) musicKByte7.stop();
        if (musicKByte8 != null) musicKByte8.stop();
//        if (musicKByte2Sound != null) musicKByte2Sound.stop();
//        if (musicKByte3Sound != null) musicKByte3Sound.stop();
//        if (musicKByte1Sound != null) musicKByte1Sound.stop();
//        if (musicKByte4Sound != null) musicKByte4Sound.stop();
//        if (musicKByte5Sound != null) musicKByte5Sound.stop();
//        if (musicKByte6Sound != null) musicKByte6Sound.stop();
//        if (musicKByte7Sound != null) musicKByte7Sound.stop();
//        if (musicKByte8Sound != null) musicKByte8Sound.stop();



        //@K—STOP ANY ANNOUNCER DIALOGUE + BEAT AUDIO ACROSS LEVELS/EXPERIMENT/TUTORIAL——————//


        if (musicNowPlayingByte1 != null) musicNowPlayingByte1.stop();
        if (musicNowPlayingByte2 != null) musicNowPlayingByte2.stop();
        if (musicNowPlayingByte3 != null) musicNowPlayingByte3.stop();
        if (musicNowPlayingByte4 != null) musicNowPlayingByte4.stop();
        if (musicNowPlayingByte5 != null) musicNowPlayingByte5.stop();
        if (musicNowPlayingByte6 != null) musicNowPlayingByte6.stop();
        if (musicNowPlayingByte7 != null) musicNowPlayingByte7.stop();
        if (musicNowPlayingByte8 != null) musicNowPlayingByte8.stop();
        if (music3AndAHalfMinutes != null) music3AndAHalfMinutes.stop();
        if (music5Minutes != null) music5Minutes.stop();
        if (music7Minutes != null) music7Minutes.stop();
        if (musicCountdown != null) musicCountdown.stop();
//        if (musicExperiment != null) musicExperiment.stop();
//        if (musicSoundset != null) musicSoundset.stop();
        if (musicTutorial != null) musicTutorial.stop();
        if (musicUnsuccessful != null) musicUnsuccessful.stop();
        if (musicSuccessful != null) musicSuccessful.stop();
        if (musicVictory != null) musicVictory.stop();


        //@K—Render Level image/sprite/texture
        batch.begin();
        levelSprite.draw(batch);
        batch.end();

        //@K—Render Level header image/sprite/texture
        batch.begin();
        levelHeaderSprite.draw(batch);
        batch.end();


        //@K—Render "LVL ONE" image/sprite/texture
        batch.begin();
        lvl1Sprite.draw(batch);
        batch.end();

        //@K—Render "LVL TWO" image/sprite/texture
        batch.begin();
        lvl2Sprite.draw(batch);
        batch.end();

        //@K—Render "LVL THREE" image/sprite/texture
        batch.begin();
        lvl3Sprite.draw(batch);
        batch.end();

        //@K—Render back arrow image/sprite/texture
        batch.begin();
        backArrowSprite.draw(batch);
        batch.end();

    }

    @Override
    public void render(float dt) {
        super.render(dt);
        //@K—MANDATORY FOR Defaults/ACTORS TO OVERLAY AND BE RENDERED AND Z-INDEXED *ON TOP OF* THE STAGE, *AFTER* THE TEXTURE/SPRITE.
        //... Invoking update simply to update the GUI; *then* overlaying/rendering the Defaults/actors with mainStage.draw() and uiStage.draw();
        //... which are overridden from the inherited class.

        //@K—main update to GUI; invocation.
        update(dt);

        mainStage.draw();
        uiStage.draw();
    }


    public boolean keyDown(int keyCode)
    {
//        if (Gdx.input.isKeyPressed(Input.Keys.ENTER))
////            setActiveScreen( new testLevel() );
//            setActiveScreen( new Level1() );
//        if(Gdx.input.isKeyPressed(Input.Keys.T))
//            setActiveScreen(new Tutorial());
        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE))
            Gdx.app.exit();

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            //✳️@K—CLEARS THE QUEUE AND TERMINATES THREAD/STOPS RUNNING TASK!✳️
            Timer.instance().clear();
            setActiveScreen(new Launch());}
        return false;
    }
}
