/**
 * @author @K—SoundByte™
 */

package com.mygdx.game;
//import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
//import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import com.badlogic.gdx.Gdx;

public class kUtils
{
    //public static Object kScreenAspectRatio;
//    public int width;
//    public int height;
    static float k4KAspectRatio= (float) (3840.0/2160.0); //1.777777778
    static int width;
    static int height;
    //@K—callback to modify app after initial launch.
    //Necessary because it must launch with a resizable=true GUI *first* to respect the Dock bounds on Mac...
    //...and other windows (all while maintaining aspect ratio).
    //...Then, after launch, lock the GUI (prevent resizing, that is).
    public interface kCallback
    {
        void kLockGUI();
        void kResize();
    }

    public static int kScreenAspectRatioWidth()
    {
        //@K—Aspect ratio calculation 1::IF MONITOR VIRTUAL DIMENSIONS ARE ≤3840X2160 (ELSE ≥ same dimensions, different calculation)
        //... while also making GUI a tad smaller to avoid Dock issues on Mac + be non-resizable.
        //... the one edge case is a fairly large WinX/Windows 10 Taskbar (as it can be resized to nearly half-screen)—must be addressed (although impractical).

        float kMatchAspectRatioWidth=3840-(k4KAspectRatio*Gdx.graphics.getWidth());

        //@K—dividing by an arbitrary (2.3 in particular below) amount to make a tad smaller to, again, avoid Dock/Taskbar/boundary issues.
        //... doesn't matter as much—can be declared "final" even given the relativity/the fact that the other calculations are ...
        //... relative/not absolute (hard-coded, that is).
        width = (int) ((kMatchAspectRatioWidth+k4KAspectRatio*Gdx.graphics.getWidth())/2.3);
//        return new int[]{width,height}; //Signature must be int[] if returning this array
        return width;
    }

    public static int kScreenAspectRatioHeight()
    {
        //@K—Aspect ratio calculation 1::IF MONITOR VIRTUAL DIMENSIONS ARE ≤3840X2160 (ELSE ≥ same dimensions, different calculation)
        //... while also making GUI a tad smaller to avoid Dock issues on Mac + be non-resizable.
        //... the one edge case is a fairly large WinX/Windows 10 Taskbar (as it can be resized to nearly half-screen)—must be addressed (although impractical).
        float kMatchAspectRatioHeight=2160-(k4KAspectRatio*Gdx.graphics.getHeight());

        //@K—dividing by an arbitrary (2.3 in particular below) amount to make a tad smaller to, again, avoid Dock/Taskbar/boundary issues.
        //... doesn't matter as much—can be declared "final" even given the relativity/the fact that the other calculations are ...
        //... relative/not absolute (hard-coded, that is).
        height = (int) ((kMatchAspectRatioHeight+k4KAspectRatio*Gdx.graphics.getHeight())/2.3);
//        return new int[]{width,height}; //Signature must be int[] if returning this array
        return height;
    }
}

/*
class kScreenAspectRatio
    {
    //    public static Object kScreenAspectRatio;
    //    public int width;
    //    public int height;

        //@K—Aspect ratio calculation 1::IF MONITOR VIRTUAL DIMENSIONS ARE ≤3840X2160 (ELSE ≥ same dimensions, different calculation)
        //... while also making GUI a tad smaller to avoid Dock issues on Mac + be non-resizable.
        //... the one edge case is a fairly large WinX/Windows 10 Taskbar (as it can be resized to nearly half-screen)—must be addressed (although impractical).
        float k4KAspectRatio=3840/2160; //1.777777778
        float kMatchAspectRatioWidth=3840-(k4KAspectRatio*Gdx.graphics.getWidth());
        float kMatchAspectRatioHeight=2160-(k4KAspectRatio*Gdx.graphics.getHeight());

        //@K—dividing by an arbitrary (2.3 in particular below) amount to make a tad smaller to, again, avoid Dock/Taskbar/boundary issues.
        //... doesn't matter as much—can be declared "final" even given the relativity/the fact that the other calculations are ...
        //... relative/not absolute (hard-coded, that is).
        int width = (int) ((kMatchAspectRatioWidth+k4KAspectRatio*Gdx.graphics.getWidth())/2.3);
        int height = (int) ((kMatchAspectRatioHeight+k4KAspectRatio*Gdx.graphics.getHeight())/2.3);
    }
*/