package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
//———————————————————————————————————————————//
//@K—CHANGED IMPORTS TO NEW BACKEND ENGINE, Lwjgl3!
//import com.badlogic.gdx.backends.lwjgl3.*;
//import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
//———————————————————————————————————————————//

//@K—necessary for callback functionality
//import com.mygdx.game.kCallback;
import com.mygdx.game.kUtils;
import com.mygdx.game.SoundByte;
//———————————————————————————————————————————//


public class DesktopLauncher {

	public static void main (String[] arg) {
		//@K—Added/declare/marked as "final" using keyword
		//@K—major render/engine alteration: SWITCH TO Lwjgl3 from regular Lwjgl
		final LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//		final Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		//———————————————————————————————————————————//

		//@K—Full 4K
//		config.width = 3840;
//		config.height = 2160;
		//———————————————————————————————————————————//

		//@K—[obsolete here, reference-only]
//		new LwjglApplication(new SoundByte(),config);
//		Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
//		config.width= (int) dimension.getWidth();
//		config.height= (int) dimension.getHeight();
		//@K—only Lwjgl (not 3!); ".width,.height,.x,.y" don't work in this engine—neither do ".resizable and ".getDesktopDisplayMode"!
		//@K—need a "window" object, as below.
		//Lwjgl3Window window = ((Lwjgl3Graphics)Gdx.graphics).getWindow();
		//———————————————————————————————————————————//

//		@K—using LWJGL3 engine back-end (for rendering and more—but completely different implementation than v2)
//		config.width = (int) (Lwjgl3ApplicationConfiguration.getDesktopDisplayMode().width);
//		config.height = (int) (Lwjgl3ApplicationConfiguration.getDesktopDisplayMode().height);
		//———————————————————————————————————————————//


		//@K—VIRTUAL DIMENSIONALITY TO NEARLY ALL OR ALL SCREENS, IDEALLY—my own calculations here.
		//...mathematical virtual dimensionality to center while account for variance in screen sizes (which is also set above...
		//...via mathematical means/formulae.
		//@K—1792x1120 is the output on MBP 16" 2019 Intel-based.
		//4k=3840x2160::involved in calculating the desired dimensionality, resolution, and aspect ratio across varying...
		//... screen resolutions/displays, etc.

//		//@K—Aspect ratio calculation 1::IF MONITOR VIRTUAL DIMENSIONS ARE ≤3840X2160 (ELSE ≥ same dimensions, different calculation)
//		//... while also making GUI a tad smaller to avoid Dock issues on Mac + be non-resizable.
//		//... the one edge case is a fairly large WinX/Windows 10 Taskbar (as it can be resized to nearly half-screen)—must be addressed (although impractical).
		float k4KAspectRatio=3840/2160; //1.777777778
		float kMatchAspectRatioWidth=3840-(k4KAspectRatio*LwjglApplicationConfiguration.getDesktopDisplayMode().width);
		float kMatchAspectRatioHeight=2160-(k4KAspectRatio*LwjglApplicationConfiguration.getDesktopDisplayMode().height);
//
//		//@K—dividing by an arbitrary (2.3 in particular below) amount to make a tad smaller to, again, avoid Dock/Taskbar/boundary issues.
//		//... doesn't matter as much—can be declared "final" even given the relativity/the fact that the other calculations are ...
//		//... relative/not absolute (hard-coded, that is).
		config.width = (int) ((kMatchAspectRatioWidth+LwjglApplicationConfiguration.getDesktopDisplayMode().width)/2.3);
		config.height = (int) ((kMatchAspectRatioHeight+LwjglApplicationConfiguration.getDesktopDisplayMode().height)/2.3);

		//@K—same as above but acquire width/height using my helper/aux[iliary] class+methods
//		config.width=kUtils.kScreenAspectRatioWidth();
//		config.width=kUtils.kScreenAspectRatioHeight();

//		System.out.println(kUtils.kScreenAspectRatioWidth()+" "+kUtils.kScreenAspectRatioHeight());
//		System.out.println(config.x+" "+config.y);
//		System.out.println(config.width+" "+config.height);
		//———————————————————————————————————————————//

//		System.out.println("ONE>>>"+config.width+"      "+config.height);
//		//@K—POSITIONING::similar to above, but for GUI window. Move down by an arbitrary amount relative to screen size/dimensionality ...
		//... to avoid colliding with Menubar on Mac/macOS, an upper-positioned WinX/Windows taskbar, etc.
//		config.x = (int) (Lwjgl3ApplicationConfiguration.getDesktopDisplayMode().width-(Lwjgl3ApplicationConfiguration.getDesktopDisplayMode().width/1.091));
//		config.y = (int) (Lwjgl3ApplicationConfiguration.getDesktopDisplayMode().height-(Lwjgl3ApplicationConfiguration.getDesktopDisplayMode().height/1.05));
//		config.x = (int) (LwjglApplicationConfiguration.getDesktopDisplayMode().width-(LwjglApplicationConfiguration.getDesktopDisplayMode().width/1.091));
		config.y = (int) (LwjglApplicationConfiguration.getDesktopDisplayMode().height-(LwjglApplicationConfiguration.getDesktopDisplayMode().height/1.05));
//		config.resizable = true;
		config.resizable = false;
//		config.fullscreen = true;
		System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
		//———————————————————————————————————————————//


		//@K—idea: rounded GUI corners—researching implementation/W.I.P/work in progress.
// 		System.out.println(LwjglApplicationConfiguration.getDesktopDisplayMode().width+" "+LwjglApplicationConfiguration.getDesktopDisplayMode().height);
//		Gdx.graphics.setUndecorated(false);
//		JFrame frame = new JFrame();
//		frame.setUndecorated(true);
//		frame.setShape(new RoundRectangle2D.Double(10, 10, 100, 100, 50, 50));
//		frame.setSize(300, 200);
//		frame.setVisible(true);
//		System.out.println("X/WIDTH: "+config.width+" • Y/HEIGHT: "+config.height);
//		try{Thread.sleep(2000);}catch(InterruptedException e){e.printStackTrace();}
		//———————————————————————————————————————————//


		//@K—render the GUI with the Lwjgl3 back-end engine
//		new Lwjgl3Application(new SoundByte(),config);
		//@K—this time, implement a callback instead of "regularly" rendering it, as above
//		new Lwjgl3Application(new SoundByte(new kCallback()
//		{
//			@Override
//			public void kLockGUI(){/*config.resizable=false;*/}
//
//			@Override
//			public void kResize(){/*config.width=3840;config.height=2160;*/}
//		}),config);
//
//		@K—regular, "vanilla" Lwjgl(2)
		new LwjglApplication(new SoundByte(new kUtils.kCallback()
		{
			@Override
			public void kLockGUI(){/*config.resizable=false;*/}

			@Override
			public void kResize(){/*config.width=3840;config.height=2160;*/}
		}),config);
		//———————————————————————————————————————————//

//		@K—tabbing in/out of fullscreen
//		callback.kLockGUI();
//		callback.kResize();
//		Gdx.graphics.setWindowedMode(3840,2160);
//		Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
//		@Override
//		public void render(float dt) {
//			super.render(dt);
//			if (Gdx.input.isKeyPressed(Input.Keys.TAB)){
//				Gdx.graphics.setFullscreenMode(Gdx.graphics.getDisplayMode());
////			Boolean fullScreen = Gdx.graphics.isFullscreen();
////			Graphics.DisplayMode currentMode = Gdx.graphics.getDisplayMode();
////			if (fullScreen == true)
////				Gdx.graphics.setWindowedMode(currentMode.width, currentMode.height);
////			else
////				Gdx.graphics.setFullscreenMode(currentMode);
//			}
//		}
		//———————————————————————————————————————————//
	}
}
